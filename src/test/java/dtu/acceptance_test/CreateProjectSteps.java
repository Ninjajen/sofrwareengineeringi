  package dtu.acceptance_test;

import dtu.application.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;

//Main responsible: s204433

public class CreateProjectSteps {
    private TestApp app;
    private ErrorMessageHolder errorMessageHolder;

    public CreateProjectSteps(TestApp app, ErrorMessageHolder errorMessageHolder) {
        this.app = app;
        this.errorMessageHolder = errorMessageHolder;
    }

    @Given("there is a developer")
    public void thereIsADeveloper() throws Exception {
        app.developer = new Developer("Some Dude");
    }

    @When("the developer creates a new project {string}")
    public void theDeveloperCreatesANewProject(String name) {
        app.project = new Project(new Info(name));
    }

    @Then("the project {string} is created")
    public void theProjectIsCreated(String name) {
        assertEquals(app.project.getInfo().getName(), name);
    }
    
    @Given("there is a project {string}")
    public void thereIsAProject(String name) {
        app.project = new Project(new Info(name));
        app.projects.add(app.project);
        assertTrue(app.projects.contains(app.project));
    }

    @When("the project manager removes {string}")
    public void theProjectManagerRemoves(String name) {
        for(Project project : app.projects) {
            if(project.getInfo().getName().equals(name)) {
                app.projects.remove(project);
                break;
            }
        }
    }

    @Then("the project {string} is deleted")
    public void theProjectIsDeleted(String name) {
        assertFalse(app.projects.stream().anyMatch(p -> p.getInfo().getName().equals(name)));
    }

    @Given("the project has no assigned project manager")
    public void theProjectHasNoAssignedProjectManager() {
        assertNull(app.project.getAssignedProjectManager());
    }

    @When("the developer assigns himself as the project manager")
    public void theDeveloperAssignsHimselfAsTheProjectManager() {
        app.projectManager = new ProjectManager(app.developer);

        app.project.setAssignedProjectManager(app.projectManager);
    }

    //@author s204455
    @Then("the developer is assigned as the project manager")
    public void theDeveloperIsAssignedAsTheProjectManager() {
        assertEquals(app.project.getAssignedProjectManager().getID(), app.developer.getID());
    }

    @Given("there is an activity which is not a part of the project")
    public void thereIsAnActivityWhichIsNotAPartOfTheProject() {
        app.activity = new Activity(ActivityType.TEST, new Info("Activity", "It is not a part of the project"));
        assertFalse(app.project.getProjectActivities().contains(app.activity));
    }
    
    @When("the project manager changes the project name to {string} and the description to {string}")
    public void theProjectManagerChangesTheProjectNameToAndTheDescriptionTo(String newName, String newDescription) {
        app.project.getInfo().setName(newName);
        app.project.getInfo().setDescription(newDescription);
    }

    @Then("the project name is {string} and the description is {string}")
    public void theProjectNameIsAndTheDescriptionIs(String name, String description) {
        assertEquals(app.project.getInfo().toString(), name + " : " + description);
    }
    
    @When("the developer gets assigned to the project")
    public void theDeveloperGetsAssignedToTheProject() {
        try {
        	app.project.addDeveloper(app.developer);
		} catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
		}
    }
    
    @Given("the developer is assigned to the project")
    public void theDeveloperIsAssignedToTheProject() throws Exception {
        app.project.addDeveloper(app.developer);
    }
    
    //@author s204455
    @When("the project manager removes the developer")
    public void theProjectManagerRemovesTheDeveloper() {
    	try {
    		app.project.removeDeveloper(app.developer);
		} catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
		}
    }
    
    //@author s204455
    @Then("the developer is no longer in the project")
    public void theDeveloperIsNoLongerInTheProject() {
    	assertFalse(app.project.getAssignedDevelopers().contains(app.developer));
    }
    
    //@author s204455
    @Given("the project manager is removed")
    public void theProjectManagerIsRemoved() throws Exception {
        app.project.removeAssignedProjectManager(app.projectManager);
        assertNull(app.project.getAssignedProjectManager());
    }
}