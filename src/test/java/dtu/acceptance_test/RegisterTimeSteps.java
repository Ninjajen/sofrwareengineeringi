package dtu.acceptance_test;

import dtu.application.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;

//Main responsible: s204433

public class RegisterTimeSteps {
    private TestApp app;

    private TimeRegister register;

    private ErrorMessageHolder errorMessageHolder;

    public RegisterTimeSteps(TestApp app, ErrorMessageHolder errorMessageHolder) {
        this.app = app;
        this.errorMessageHolder = errorMessageHolder;
    }

    @Given("the project has an activity")
    public void theProjectHasAnActivity() throws Exception {
        app.activity = new Activity(ActivityType.TEST, new Info("Time Activity", "Activity for testing the time registration feature"));

        app.project.addActivity(app.activity);
    }

    @Given("the activity has a developer")
    public void theActivityHasADeveloper() throws Exception {
        app.developer = new Developer("Timmy Time");

        app.activity.addDeveloper(app.developer);
    }

    @Given("the developer has no previous time registered")
    public void theDeveloperHasNoPreviousTimeRegistered() {
        assertFalse(app.project.getTimeRegister().stream().anyMatch(c -> c.getDeveloper() == app.developer));
    }

    @Given("the activity has no previous time registered")
    public void theActivityHasNoPreviousTimeRegistered() {
        assertFalse(app.project.getTimeRegister().stream().anyMatch(c -> c.getActivity() == app.activity));
    }

    @Then("the developer has a registered time of {int} hours")
    public void theDeveloperHasARegisteredTimeOfHours(int time) {
        int registeredTime = 0;
        for(TimeRegister commit : app.project.getTimeRegister()) {
            if(commit.getDeveloper() == app.developer) {
                registeredTime += commit.getDuration();
            }
        }

        assertEquals(registeredTime, time);
    }

    @Then("the activity has a registered time of {int} hours")
    public void theActivityHasARegisteredTimeOfHours(int time) {
        int registeredTime = 0;
        for(TimeRegister commit : app.project.getTimeRegister()) {
            if(commit.getActivity() == app.activity) {
                registeredTime += commit.getDuration();
            }
        }

        assertEquals(registeredTime, time);
    }

    @Given("the activity has no developers")
    public void theActivityHasNoDevelopers() {
        assertEquals(app.activity.getAssignedDevelopers().size(), 0);
    }

    @Given("the developer is not assigned to the activity")
    public void theDeveloperIsNotAssignedToTheActivity() {
        assertFalse(app.activity.getAssignedDevelopers().contains(app.developer));
    }

    @When("the developer registers {int} hours worked on the activity")
    public void theDeveloperRegistersHoursWorkedOnTheActivity(Integer duration) {
        register = new TimeRegister(app.developer, app.activity, duration);

        try {
            app.project.registerTime(register);
        } catch(Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }

    @When("the developer edits the registered time by adding {int} hours worked on the activity")
    public void theDeveloperEditsTheRegisteredTimeByAddingHoursWorkedOnTheActivity(int amount) {
        try {
            register.setDuration(register.getDuration() + amount);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }
}