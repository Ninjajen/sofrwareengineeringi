package dtu.acceptance_test;

import static org.junit.Assert.assertTrue;

import dtu.application.Info;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

//@author s204455
public class ActivityToStringSteps {

	private Info info;
	private String infoString;
	
	public String toString() {
		String infoString;

		if(info != null) {
			if(!info.getName().equals("")) {
				infoString = info.getName();
			} else {
				infoString = "Unnamed Activity";
			}

			if(!info.getDescription().equals("")) {
				infoString += " : " + info.getDescription();
			} else {
				infoString += " : No Description";
			}
		} else {
			infoString = "Unnamed Activity : No Description";
		}

		return infoString;
	}
	
	@Given("the activity has the name {string} and description {string}")
	public void the_activity_has_the_name_and_description(String name, String description) {
	    info = new Info(name, description);
	}

	@When("the toString is found")
	public void the_to_string_is_found() {
		infoString = toString();
	}

	@Then("the description is {string}")
	public void the_description_is(String string) {
	    assertTrue(infoString.equals(string));
	}
 
	@Given("the info is not there")
	public void the_info_is_not_there() {
	    info = null;
	}
}
