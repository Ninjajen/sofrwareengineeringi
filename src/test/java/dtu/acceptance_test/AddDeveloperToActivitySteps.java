package dtu.acceptance_test;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import dtu.application.*;

/**
 * @author s204507, for the entire class
*/

public class AddDeveloperToActivitySteps {
	
	private TestApp app;
	private ErrorMessageHolder errorMessageHolder;
	
	public AddDeveloperToActivitySteps (TestApp app, ErrorMessageHolder errorMessageHolder) throws Exception {
		this.app = app;
		this.errorMessageHolder = errorMessageHolder;
		
        app.developer = new Developer("Anita Dolly Eriksen Vildmark");
	}
	
	@Given("there is an activity within the project")
	public void thereIsAnActivityWithinTheProject() throws Exception {
        app.activity = new Activity(ActivityType.TEST, app.info);
        app.project.addActivity(app.activity);
		
		assertNotNull(app.project.getProjectActivities());
	}
	
	@Given("there are no other developers assigned to the activity")
	public void thereAreNoOtherDevelopersAssignedToTheActivity() {
		assertTrue(app.project.getAssignedDevelopers().isEmpty());
	}

	@When("the project manager assigns the developer {string}")
	public void theProjectManagerAssignsTheDeveloper(String string) throws Exception {
        try {
        	app.projectManager.addDeveloperToActivity(app.developer, app.activity);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
	}

	@Then("the developer is added to the project activity")
	public void theDeveloperIsAddedToTheProjectActivity() {
		devIsAssigned();
	}

	@Given("the developer {string} already is assigned to the activity")
	public void thDeveloperAlreadyIsAssignedToTheActivity(String string) throws Exception {
		app.projectManager.addDeveloperToActivity(app.developer, app.activity);
		devIsAssigned();
	}

	@When("the project manager removes the developer {string} from the activity")
	public void theProjectManagerRemovesTheDeveloperFromTheActivity(String string) throws Exception {
        try {
        	app.activity.removeDeveloper(app.developer);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
	}

	@Then("the developer is removed from the project activity")
	public void theDeveloperIsRemovedFromTheProjectActivity() {
		assertFalse(app.project.getAssignedDevelopers().contains(app.developer));
	}
	
	private void devIsAssigned() {
		assertTrue(app.activity.getAssignedDevelopers().contains(app.developer));
	}
}