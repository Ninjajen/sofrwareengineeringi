package dtu.acceptance_test;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import dtu.application.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateActivitySteps {
	private TestApp app;

	private ErrorMessageHolder errorMessageHolder;
	
    public CreateActivitySteps(TestApp app, ErrorMessageHolder errorMessageHolder) {
        this.app = app;
        this.errorMessageHolder = errorMessageHolder;
    }
    
    //@author s204488
    @Given("there is a project")
    public void thereIsAProject() {
        app.info = new Info("Test", "For testing purposes only");
        app.project = new Project(app.info);
    }

    //@author s204488
    @Given("there is a project manager")
    public void thereIsAProjectManager() throws Exception {
        app.developer = new Developer("John Doe");
        app.projectManager = new ProjectManager(app.developer);
        app.project.setAssignedProjectManager(app.projectManager);
        
        assertTrue(app.project.authenticateProjectLeader(app.projectManager));
    }
    
   //@author s204488
    @When("an activity is created within the project")
    public void anActivityIsCreatedWithinTheProject() throws Exception {
        app.activity = new Activity(ActivityType.TEST, app.info);
        app.project.addActivity(app.activity);
    }
    
    //@author s204488
    @Then("the activity exists in the project")
    public void theActivityExistsInTheProject() {
        assertEquals(app.project.getActivityByID(app.activity.getID()), app.activity);
    }
    
    //@author s204507
    @Then("the project has a project ID")
    public void theProjectHasAProjectID() {
        assertNotNull(app.project.getID());
    }
    
    //@author s204507
    @Then("the activityID is unique within the project")
    public void theActivityIDIsUniqueWithinTheProject() {
    	assertTrue(app.project.validateUniqueActivityID(app.activity.getID()));
    }
    
    //@author s204488
    @Given("the project has a developer")
    public void developerInProject() throws Exception {
        app.developer = new Developer("John Doe");
        app.projectManager = new ProjectManager(app.developer);
        app.project.setAssignedProjectManager(app.projectManager);
        app.project.addDeveloper(app.developer);
    }
    
    //@author s204455
    @Given("there is another developer who is not the project manager")
    public void thereIsAnotherDeveloperWhoIsNotTheProjectManager() throws Exception {
        app.developers.add(app.developer);
        app.projectManagers.add(app.projectManager);
        app.developers.add(new Developer("Hans Zimmer"));
        app.projectManagers.add(new ProjectManager(app.developers.get(1)));
        assertFalse(app.project.authenticateProjectLeader(app.projectManagers.get(1)));
    }
    
    //@author s204455 and s204488
    @When("the developer creates an activity")
    public void developerCreatesActivity() {
    app.project.setAssignedProjectManager(null);	
    	try {
            app.activity = new Activity(ActivityType.TEST, app.info);
            app.project.addActivity(app.activity);
		} catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
		}
    }
    
    //@author s204488
    @Then("the error message {string} is given")
    public void errorMessageIsGiven(String errorMessage) {
    	assertEquals(errorMessage, errorMessageHolder.getErrorMessage());
    }
}