package dtu.acceptance_test;

//All made by s204455

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.Assert.*;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTestSteps {
	private LocalDate date1;
	private LocalDate date2;
	private long timeBetweenDates;
//--------------------------------Create one date with just day and month -------------------------
	@When("a date is created with day {int} and month {int}")
	public void aDateIsCreatedWithDayAndMonth(Integer day, Integer month) {
		date1 = LocalDate.of(LocalDate.now().getYear(), month, day);
	}
	
	@Then("the date is at the {int} day and the {int} month")
	public void theDateIsAtTheDayAndTheMonth(Integer day, Integer month) {
		assertEquals((int) day, date1.getDayOfMonth());
		assertEquals((int) month, date1.getMonthValue());
	}

	@Then("the date has the toString {string}")
	public void theDateHasTheToString(String date) {
		assertEquals(date1.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")), date);
	}

	//--------------------------------Create one date with day, month and year-------------------------
	@When("a date is created with day {int} and month {int} and the year {int}")
	public void aDateIsCreatedWithDayAndMonthAndTheYear(Integer day, Integer month, Integer year) {
	    date1 = LocalDate.of(year, month, day);
	}

	@Then("the date is at the {int} day and the {int} month and the {int} year")
	public void theDateIsAtTheDayAndTheMonthAndTheYear(Integer day, Integer month, Integer year) {
		assertEquals(date1.getDayOfMonth(), (int) day);
		assertEquals(date1.getMonthValue(), (int) month);
		assertEquals(date1.getYear(), (int) year);
	}
	
	//--------------------------------Comparing two days of just day and month-------------------------
	@Given("there is a date with day {int} and month {int}")
	public void thereIsADateWithDayAndMonth(Integer day, Integer month) {
	    date1 = LocalDate.of(LocalDate.now().getYear(), month, day);
	}
	
	@Given("there is another date with day {int} and month {int}")
	public void thereIsAnotherDateWithDayAndMonth(Integer day, Integer month) {
	   date2 = LocalDate.of(LocalDate.now().getYear(), month, day);
	}

	@When("the two dates are compared")
	public void theTwoDatesAreCompared() {
		timeBetweenDates = DAYS.between(date1, date2);
	}

	@Then("the date difference is {int}")
	public void theDateDifferenceIs(Integer diff) {
		assertEquals(timeBetweenDates, (int) diff);
	}
	
	//--------------------------------Comparing two days of day, month and year-------------------------
	@Given("there is a date with day {int} and month {int} and year {int}")
	public void thereIsADateWithDayAndMonthAndYear(Integer day, Integer month, Integer year) {
	    date1 = LocalDate.of(year, month, day);
	}
	
	@Given("there is another date with day {int} and month {int} and year {int}")
	public void thereIsAnotherDateWithDayAndMonthAndYear(Integer day, Integer month, Integer year) {
	    date2 = LocalDate.of(year, month, day);
	}	
	
	//--------------------------------Changing dates---------------------------------------------------
	@When("the date is changed to the {int} day and the {int} month")
	public void theDateIsChangedToTheDayAndTheMonth(Integer day, Integer month) {
		date1 = LocalDate.of(date1.getYear(), month, day);
	}
	
	@When("the date is changed to the {int} day and the {int} month and year {int}")
	public void theDateIsChangedToTheDayAndTheMonthAndYear(Integer day, Integer month, Integer year) {
		date1 = LocalDate.of(year, month, day);
	}
}