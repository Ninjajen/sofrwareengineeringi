package dtu.acceptance_test;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import dtu.application.*;
import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

//Main responsible: s204455

//-------------------------------------------------------------Isolated steps--------------------------------------
public class AskForAssistanceSteps {
	private TestApp app;
	private ErrorMessageHolder errorMessageHolder;
	
	public AskForAssistanceSteps(TestApp app, ErrorMessageHolder errorMessageHolder) {
		this.app = app;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	//-------------------------------Initializing company developers ----------------------------- made by s204455
	@Given("the standard developers are in working at the company")
	public void theStandardDevelopersAreInWorkingAtTheCompany() throws Exception {
		app.developers.add(new Developer("ADEV"));
	    app.developers.add(new Developer("HELP"));
	    app.developers.add(new Developer("BOSS"));
	    app.developers.add(new Developer("CHEF"));
	}
	
	//-------------------------------The company has a set of developers-------------------------- made by s204455
	@Given("developer {string} is assigned to the activity")
	public void developerIsAssignedToTheActivity(String name) throws Exception {
		app.developer = app.developers.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);

		assertNotNull(app.developer);

		app.project.getActivityByID(app.activity.getID()).addDeveloper(app.developer);

		assertTrue(app.activity.getAssignedDevelopers().stream().anyMatch(p -> p.getName().equals(name)));
	}
	
	@Given("developer {string} is available")
	public void developerIsAvailable(String name) {
		app.date = LocalDate.now().plusDays(1);
		app.developer = app.developers.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);

		assertNotNull(app.developer);

		app.developer.setNumActiveActivities(0);
		app.developer.removeAllActivityEndDate();

		assertTrue(app.developer.isAvailable(app.date));
	}
	
	@Given("no other developers than {string} are available")
	public void noOtherDevelopersThanAreAvailable(String name) {
		app.date = LocalDate.now().plusDays(1);
		for (int i = 0; i < app.developers.size(); i++) {
	    	if(app.developers.get(i).getName().equals(name)) {
	    		continue;
	    	}
	    	for (int j = 0; j < 20; j++) {
	    		app.developers.get(i).addActivityEndDate(LocalDate.now().plusDays(5));
	    		app.developers.get(i).setNumActiveActivities(app.developers.get(i).getNumActiveActivities() + 1);
	    	}
	    }
	    
	    for (int i = 0; i < app.developers.size(); i++) {
	    	if(app.developers.get(i).getName().equals(name)) {
	    		continue;
	    	}
	    	assertFalse(app.developers.get(i).isAvailable(app.date));
	    }
	}
	
	@When("the assigned developer {string} asks for assistance on the activity")
	public void theAssignedDeveloperAsksForAssistanceOnTheActivity(String name) throws Exception {
	    Developer asking = app.developers.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);
	    if(asking != null) {
	    	app.developer = asking.askForAssistance(app.activity, app.developers);
		} else {
			assertNotNull(app.developer);
		}
	}
	
	@Then("the developer {string} assists the assigned developer {string} on the activity")
	public void theDeveloperAssistsTheAssignedDeveloperOnTheActivity(String assisting, String string2) {
		assertTrue(app.activity.getAssistingDevelopers().contains(app.developer));
		assertEquals(app.developer.getName(), assisting);
	}
	
	//--------------------------------------------No developer to get help from-----------------------------
	
	@Then("the assigned developer receives no help on the activity")
	public void theAssignedDeveloperReceivesNoHelpOnTheActivity() {
		assertEquals(0, app.activity.getAssistingDevelopers().size());
	}
	
	//---------------------------------------------The developer asks for help on the wrong activity-----------
	@Given("the developer {string} is assigned to the activity {string}")
	public void theDeveloperIsAssignedToTheActivity(String developer, String activity) throws Exception {
	    Developer assigned = app.developers.stream().filter(d -> d.getName().equals(developer)).findFirst().orElse(null);
	    Activity assignedTo = app.project.getProjectActivities().stream().filter(a -> a.getInfo().getName().equals(activity)).findFirst().orElse(null);
	    if (assigned != null && assignedTo != null) {
	    	assignedTo.addDeveloper(assigned);
	    }
	}
	
	@When("the assigned developer {string} asks for assistance on activity {string}")
	public void theAssignedDeveloperAsksForAssistanceOnActivity(String developer, String activity) {
	    Activity assignedTo = app.project.getProjectActivities().stream().filter(a -> a.getInfo().getName().equals(activity)).findFirst().orElse(null);
	    Developer asking = app.developers.stream().filter(d -> d.getName().equals(developer)).findFirst().orElse(null);
	    try {
			asking.askForAssistance(assignedTo, app.developers);
		} catch (Exception e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	//---------------------------------------------The developer asks for help when one or more developers are available-----------
	@Given("no other developers than {string} and {string} are available")
	public void noOtherDevelopersThanAndAreAvailable(String developer1, String developer2) {
		app.date = LocalDate.now().plusDays(1);
		for (int i = 0; i < app.developers.size(); i++) {
	    	if(app.developers.get(i).getName().equals(developer1) || app.developers.get(i).getName().equals(developer2)) {
	    		continue;
	    	}
	    	for (int j = 0; j < 20; j++) {
	    		app.developers.get(i).addActivityEndDate(LocalDate.now().plusDays(5));
	    		app.developers.get(i).setNumActiveActivities(app.developers.get(i).getNumActiveActivities() + 1);
	    	}
	    }
	    
	    for (int i = 0; i < app.developers.size(); i++) {
	    	if(app.developers.get(i).getName().equals(developer1) || app.developers.get(i).getName().equals(developer2)) {
	    		continue;
	    	}
	    	assertFalse(app.developers.get(i).isAvailable(app.date));
	    }
	}
	
	@Then("the assigned developer receives assistance from either the developer {string} or the developer {string}")
	public void theAssignedDeveloperReceivesAssistanceFromEitherTheDeveloperOrTheDeveloper(String dev1, String dev2) {
		assertTrue(app.activity.getAssistingDevelopers().get(0).getName().equals(dev1) 
				|| app.activity.getAssistingDevelopers().get(0).getName().equals(dev2));
	}
}