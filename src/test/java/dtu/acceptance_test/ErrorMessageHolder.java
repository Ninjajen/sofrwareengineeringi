package dtu.acceptance_test;

//@author Hubert, from libraryApp

public class ErrorMessageHolder {
	private String errorMessage = "";

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}