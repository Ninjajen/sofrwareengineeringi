package dtu.acceptance_test;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import dtu.application.Developer;
import dtu.application.TestApp;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * This class tests ID Generation the Developer.
 *
 * @author s204057, for entire class
*/

public class CreateDeveloperSteps {

	private String name;
	private Developer developer;
	private String id = "";
	private TestApp app;
	private ErrorMessageHolder errorMessageHolder;
	
	public CreateDeveloperSteps(TestApp app, ErrorMessageHolder errormessageholder) {
		this.app = app;
		this.errorMessageHolder = errormessageholder;
	}
	
	@Given("there is a person with the name {string}")
	public void thereIsAPersonWithTheName(String string) throws Exception {
		
		try {
			if (developer.validName(string)) {
				name = string;
			}
			assertEquals(name, string);
		} catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
	}

	@Given("there exists no other developers with the ID {string}")
	public void thereExistsNoOtherDevelopersWithTheId(String string) throws Exception {
		id = Developer.generateID(name);
		
		// uniqueDevID returns true if id exists as the only one of its value
		// this has to be false since the ID cannot be anywhere in the devList
		assertFalse(uniqueDevID(string)); 
	}

	@When("the person is added to the company system")
	public void thePersonIsAddedToTheCompanySystem() throws Exception {
		developer = new Developer(name);
		app.addDeveloper(developer);
	}

	@Then("the developer is assigned a unique ID")
	public void theDeveloperIsAssignedAUniqueId() {
		for (Developer d : app.developers) {
			assertNotEquals(d.getID(), id);
		}
	}
	
	@Then("the first chars are the initials {string}")
	public void theFirstCharsAreTheInitials(String string) {
		assertEquals(id.substring(0, string.length()), string);
	}
	
	@Then("the person is in the company system")
	public void thePersonIsInTheCompanySystem() {
		assertTrue(app.developers.contains(developer));
	}
	
	@Then("the developer is assigned a unique ID {string}")
	public void theDeveloperIsAssignedAUniqueId(String string) {
		assertEquals(developer.getID(), string);
		assertTrue(uniqueDevID(string));
	}
	
	@Given("there already exists a developer with the Initials {string}")
	public void thereAlreadyExistsADeveloperWithTheInitials(String string) throws Exception {
		app.addDeveloper(developer = new Developer(name));
		
		boolean b = false;
		
		for(Developer d : app.developers) {
			if (d.getID().substring(0,string.length()).equals(string)) {
				b = true;
			}
		}
		assertTrue(b);
	}

	@Then("the developer is assigned a unique ID of four characters")
	public void theDeveloperIsAssignedAUniqueIdOfFourCharacters() {
		assertTrue(uniqueDevID(developer.getID()) && developer.getID().length() == 4);
	}
	
	private boolean uniqueDevID(String string) {
		int counter = 0;
		
		for (Developer d : app.developers) {
			if (d.getID().equals(string)) {
				counter++;
			}
		}
		return counter == 1;
	}
}