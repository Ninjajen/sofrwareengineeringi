package dtu.acceptance_test;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import dtu.application.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

//Main responsible: s204433

public class ProjectSchedulingSteps {
	private TestApp app;
	private DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");;

	public ProjectSchedulingSteps(TestApp app) {
		this.app = app;
	}

	@Given("there are no activities in the project")
	public void thereAreNoActivitiesInTheProject() {
		assertNotNull(app.project);
		app.project.getProjectActivities().clear();
		assertEquals(0, app.project.getProjectActivities().size());
	}

	@When("the project manager creates an activity with start date {string} and end date {string}")
	public void theProjectManagerCreatesAnActivityWithStartDateAndEndDate(String startdate, String enddate) throws Exception {
		app.activity = new Activity(ActivityType.TEST, new Info("Test Activity", "Testing"));

		app.activity.setStartDate(LocalDate.parse(startdate, format));
		app.activity.setEndDate(LocalDate.parse(enddate, format));

		app.project.addActivity(app.activity);

		assertTrue(app.project.getProjectActivities().contains(app.activity));
	}

	@Then("the project duration is {long}")
	public void theProjectDurationTimeIs(long duration) {
		app.project.updateDuration();
		assertEquals(duration, app.project.getProjectDuration());
	}

	@When("the project manager changes the end date of the activity to {string}")
	public void theProjectManagerChangesTheEndDateOfTheActivityTo(String enddate) {
		app.activity.setEndDate(LocalDate.parse(enddate, format));
	}

	@When("the developer creates a project with start date {string}")
	public void theDeveloperCreatesAProjectWithStartDate(String startdate) {
		app.project = new Project(new Info("Test", "Project with start date " + startdate));

		app.project.setStartDate(LocalDate.parse(startdate, format));
	}

	@Then("the project start date is {string}")
	public void theProjectStartDateIs(String startdate) {
		assertEquals(startdate, app.project.getStartDate().format(format));
	}

	@When("the project manager sets the end date to {string}")
	public void theProjectManagerSetsTheEndDateTo(String enddate) {
		app.project.setEndDate(LocalDate.parse(enddate, format));
	}

	@Then("the project end date is {string}")
	public void theProjectEndDateIs(String enddate) {
		assertEquals(enddate, app.project.getEndDate().format(format));
	}

	@Given("there is a project with a end date {string}")
	public void thereIsAProjectWithAEndDate(String enddate) {
		app.project = new Project(new Info("Test", "Project with end date " + enddate));
		theProjectManagerSetsTheEndDateTo(enddate);
	}

	@When("the project manager changes the end date to {string}")
	public void theProjectManagerChangesTheEndDateTo(String enddate) {
		app.project.setEndDate(LocalDate.parse(enddate, format));
	}

	@When("the project manager sets the activity start date to {string} and the end date to {string}")
	public void theProjectManagerSetsTheActivityStartDateToAndTheEndDateTo(String startDate, String endDate) {
		app.activity.setStartDate(LocalDate.parse(startDate, format));
		app.activity.setEndDate(LocalDate.parse(endDate, format));
	}

	@Then("the activity duration is {int}")
	public void theActivityDurationIs(int duration) {
		assertEquals(app.activity.getDuration(), duration);
	}

	@When("the project manager sets the activity's expected work hours to {int}")
	public void theProjectManagerSetsTheActivitySExpectedWorkHoursTo(int hours) throws Exception {
		app.activity.setExpectedHours(hours);
	}

	@Then("the project has {int} hours of expected work")
	public void theProjectHasHoursOfExpectedWork(int hours) {
		assertEquals(app.project.getExpectedHours(), hours);
	}
}