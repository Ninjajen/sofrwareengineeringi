package dtu.acceptance_test;

import dtu.application.Activity;
import dtu.application.ActivityType;
import dtu.application.Info;
import dtu.application.TestApp;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

import java.time.LocalDate;
import java.util.SortedSet;
import java.util.TreeSet;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UpdateDurationSteps {
    private TestApp app;

    public UpdateDurationSteps(TestApp app) {
        this.app = app;
    }

    @Given("the project has no activities")
    public void theProjectHasNoActivities() {
        assertEquals(0, app.project.getProjectActivities().size());
    }

    @When("the project duration is updated")
    public void theProjectDurationIsUpdated() {
        SortedSet<LocalDate> dates = new TreeSet<>();
        for(Activity activity : app.project.getProjectActivities()) {
            if(activity.getStartDate() != null) dates.add(activity.getStartDate());
            if(activity.getEndDate() != null) dates.add(activity.getEndDate());
        }

        if(dates.size() > 1) app.project.setProjectDuration(DAYS.between(dates.first(), dates.last()));
    }

    @Given("the project has some activities with no start- or end-dates")
    public void theProjectHasSomeActivitiesWithNoStartOrEndDates() {
        for(int i = 0; i < 5; i++) {
            Activity activity = new Activity(ActivityType.TEST, new Info("Activity " + i, "Test activity " + i));
            assertNull(activity.getStartDate());
            assertNull(activity.getEndDate());

            app.project.getProjectActivities().add(activity);
        }
    }

    @Given("the project has an activity with start date {string} and end date {string}")
    public void theProjectHasAnActivityWithStartDateAndEndDate(String startdate, String enddate) {
        Activity activity = new Activity(ActivityType.TEST, new Info("Activity", "Has a start date and an end date"));
        activity.setStartDate(LocalDate.parse(startdate));
        activity.setEndDate(LocalDate.parse(enddate));

        app.project.getProjectActivities().add(activity);
    }

    @Given("the project has an activity with start date {string}")
    public void theProjectHasAnActivityWithStartDate(String startdate) {
        Activity activity = new Activity(ActivityType.TEST, new Info("Activity", "Has a start date"));
        activity.setStartDate(LocalDate.parse(startdate));

        app.project.getProjectActivities().add(activity);
    }

    @Given("the project has an activity with end date {string}")
    public void theProjectHasAnActivityWithEndDate(String enddate) {
        Activity activity = new Activity(ActivityType.TEST, new Info("Activity", "Has an end date"));
        activity.setEndDate(LocalDate.parse(enddate));

        app.project.getProjectActivities().add(activity);
    }
}
