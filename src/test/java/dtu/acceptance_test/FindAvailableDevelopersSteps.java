package dtu.acceptance_test;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import dtu.application.*;

//All made by s204455
public class FindAvailableDevelopersSteps {
	private TestApp app;
	
	public FindAvailableDevelopersSteps(TestApp app) {
		this.app = app;
	}
	
	//------------------------------------------------------------ The project manager searches successfully for available developers -----------------------

	@Given("{string} and {string} are available developers")
	public void andAreAvailableDevelopers(String name1, String name2) {
	    Developer dev1 = app.developers.stream().filter(d -> d.getName().equals(name1)).findFirst().orElse(null);
	    Developer dev2 = app.developers.stream().filter(d -> d.getName().equals(name2)).findFirst().orElse(null);
	    assertNotNull(dev1);
	    assertNotNull(dev2);
	    dev1.removeAllActivityEndDate();
	    dev2.removeAllActivityEndDate();
	}

	@When("the project manager searches for available developers")
	public void theProjectManagerSearchesForAvailableDevelopers() {
	    app.availableDevelopers = app.projectManager.SeeAvailability(app.developers, LocalDate.now());
	}

	@Then("a list of available developers, containing {string} and {string} is returned")
	public void aListOfAvailableDevelopersContainingAndIsReturned(String name1, String name2) {
	    assertTrue(app.availableDevelopers.stream().anyMatch(d -> d.getName().equals(name1)));
	    assertTrue(app.availableDevelopers.stream().anyMatch(d -> d.getName().equals(name2)));
	}
	
	//--------------------------------------------------------------------------The project manager searches unsuccessfully for available developers-----------
	@Given("there are no available developers")
	public void thereAreNoAvailableDevelopers() {
	    for (Developer d : app.developers) {
	    	for(int i = 0; i < 25; i++) {
	    		d.addActivityEndDate(LocalDate.now().plusDays(5));
	    	}
	    	assertFalse(d.isAvailable(LocalDate.now()));
	    }
	}

	@Then("an empty list is returned")
	public void anEmptyListIsReturned() {
	    assertTrue(app.availableDevelopers.isEmpty());
	}
	
	//---------------------------------------------------The project manager adds a developer to the project--------------------------

	@Given("all developers are available")
	public void allDevelopersAreAvailable() {
	    for (Developer d : app.developers) {
	    	d.removeAllActivityEndDate();
	    	assertTrue(d.isAvailable(LocalDate.now()));
	    }
	}
	
	@When("the project manager adds the developer {string} from the list of available developers to the project")
	public void theProjectManagerAddsTheDeveloperFromTheListOfAvailableDevelopersToTheProject(String name) throws Exception {
	    app.developer = app.developers.stream().filter(d -> d.getName().equals(name)).findAny().orElse(null);
	    app.project.addDeveloper(app.developer);
	}

	@Then("the developer is added to the project")
	public void theDeveloperIsAddedToTheProject() {
	    assertTrue(app.project.getAssignedDevelopers().stream().anyMatch(d -> d.equals(app.developer)));
	}
}