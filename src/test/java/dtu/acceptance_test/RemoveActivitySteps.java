package dtu.acceptance_test;

import dtu.application.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.time.LocalDate;

import static org.junit.Assert.*;

//Made by s204455 And s204488
public class RemoveActivitySteps {
    private TestApp app;

    private ErrorMessageHolder errorMessageHolder;

    public RemoveActivitySteps(TestApp app, ErrorMessageHolder errorMessageHolder) {
        this.app = app;
        this.errorMessageHolder = errorMessageHolder;
    }

    @When("the project manager removes the activity {string}")
    public void theProjectManagerRemovesTheActivity(String name) throws Exception {
    	app.activity = app.project.getActivityByID(app.id);
    	app.project.removeActivity(app.activity);
    }
    
  @Given("the project has a project manager")
  public void theProjectHasAProjectManager() throws Exception {
      if(app.projectManager == null) {
          app.projectManager = new ProjectManager(new Developer("Project Manager"));
      }

      app.project.setAssignedProjectManager(app.projectManager);
  }

    @Then("the activity {string} is removed from the project")
    public void theActivityIsRemovedFromTheProject(String name) {
        assertFalse(app.project.getProjectActivities().stream().anyMatch(a -> a.getInfo().getName().equals(name)));
    }
    
    @Then("developer {string} do not have the activity active")
    public void developerDoNotHaveTheActivityActive(String name) {
    	Developer dev = app.developers.stream().filter(d -> d.getName().equals(name)).findAny().orElse(null);

    	assertNotNull(dev);
        assertFalse(dev.getAllActivityEndDate().stream().anyMatch(d -> d.toString().equals(app.date.toString())));
    }

    @Given("the project has an activity named {string} and type {string}")
    public void theProjectHasAnActivityNamedAndType(String name, String type) throws Exception {
        // Create a temporary Project Manager that can add the activity to the project
        if(app.project.getAssignedProjectManager() == null) {
        	app.projectManager = new ProjectManager(new Developer("Temporary Project Manager"));
            app.project.setAssignedProjectManager(app.projectManager);
        }

        app.activity = new Activity(ActivityType.valueOf(type), new Info(name));
        app.activity.setEndDate(LocalDate.now().plusDays(1));
        app.date = app.activity.getEndDate();
        app.project.addActivity(app.activity);
        app.id = app.activity.getID();
    }

    @Given("that a developer is assigned to that project")
    public void thatADeveloperIsAssignedToThatProject() throws Exception {
        if(app.developer == null) {
            app.developer = new Developer("A New Developer");
        }

        // Create a temporary Project Manager that can add the developer to the project
        if(app.project.getAssignedProjectManager() == null) {
            app.projectManager = new ProjectManager(new Developer("Temporary Project Manager"));
            app.project.setAssignedProjectManager(app.projectManager);
        }
        app.project.addDeveloper(app.developer);
    }

    @When("the developer removes the activity")
    public void theDeveloperRemovesTheActivity() {
        // Make sure the project doesn't have a project manager or remove the temporary project manager
        app.project.setAssignedProjectManager(null);

        try {
            app.project.removeActivity(app.activity);
        } catch (Exception e) {
            errorMessageHolder.setErrorMessage(e.getMessage());
        }
    }
    
    @Given("the developer {string} is assigned to that project")
    public void theDeveloperIsAssignedToThatProject(String name) throws Exception {
        Developer dev = app.developers.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);
        assertNotNull(dev);
        app.developer = dev;
        app.project.addDeveloper(dev);
    }
    
    @Then("the developer has no active activities or activity end dates")
    public void theDeveloperHasNoActiveActivitiesOrActivityEndDates() {
    	assertEquals(0, app.developer.getAllActivityEndDate().size());
    	assertEquals(0, app.developer.getNumActiveActivities());
    }

    @Given("the activity has a developer who is assigned to other activities with the same end date")
    public void theActivityHasADeveloperWhoIsAssignedToOtherActivitiesWithTheSameEndDate() throws Exception {
    	Activity newActivity = new Activity(ActivityType.valueOf("TEST"), new Info("Isolated Test Activity"));
    	newActivity.setEndDate(app.activity.getEndDate());
    	newActivity.addDeveloper(app.developer);
    	app.integerBuffer = app.developer.getNumActiveActivities();
    }
    
    @Then("the developer has an activity with this date")
    public void theDeveloperHasAnActivityWithThisDate() {
        assertTrue(app.developer.getAllActivityEndDate().stream().anyMatch(a -> a.toString().equals(app.date.toString())));
    }
    
    @Then("the developer has one less active activity")
    public void theDeveloperHasOneLessActiveActivity() {
    	assertEquals(app.integerBuffer-1, app.developer.getNumActiveActivities());
    }
}