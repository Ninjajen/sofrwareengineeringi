package dtu.acceptance_test;

import static org.junit.Assert.assertTrue;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

//Main responsible: s204488

public class ValidateNameSteps {
	private String name;
	private ErrorMessageHolder errorMessageHolder;
	private boolean output;
	
	public ValidateNameSteps(ErrorMessageHolder errorMessageHolder) {
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@Given("there is a name {string}")
	public void thereIsAName(String name) {
		this.name = name;
	}
	
	@When("the name is validated")
	public void theNameIsValidated() {
		try {
			this.output = this.validateName(name);
		} catch (Exception e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	}
	
	@Then("the method returns true")
	public void theMethodReturnsTrue() {
		assertTrue(this.output);
	}
	
	public boolean validateName(String name) throws Exception {
		
		if(name.trim().length() == 0) {						//1
			throw new Exception("Illegal Name Length 0");
		} else {											//2
			return true;
		}
	}
}