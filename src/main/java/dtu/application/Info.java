/**
 * This class loads in data from a given file and adds it to an arraylist.
 *
 * @author s204507
*/

package dtu.application;

public class Info {
    private String name;
    private String description;

    public Info(String name) {
        this.name = name;
    }

    public Info(String name, String description) {
        this.name = name;
        this.description = description;
    }

    // ----- Getters and Setters ----- // 
    
    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() { 
    	return this.name; 
    }

    public String getDescription() { 
    	return this.description;
    }

    public String toString() {
        return name + " : " + description;
    }
}
