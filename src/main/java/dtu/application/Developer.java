package dtu.application;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

//Main responsible: s204455
public class Developer {
	private String id;
	private String name;

	private int numActiveActivities = 0;
	private List<LocalDate> activeActivitiesEndDates = new ArrayList<>();;
	
	public Developer(String name) throws Exception {
		this.name = name;
		this.id = generateID(name);
	}

	// ----- ID Generation ----- //
	// made by s204507
    public static String generateID(String name) throws Exception {
		validName(name);

		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
		Random random = new Random();

		StringBuilder id = new StringBuilder();

		// Split name after whitespace
		String[] parts = name.split("\\s+");

		// first char in each name
		for(int i = 0; i < parts.length && i < 4; i++) {
			id.append(parts[i].charAt(0));
		}

		// if id length != 4, add random char until length = 4
		while(id.length() < 4) {
			id.append(alphabet.charAt(random.nextInt(alphabet.length())));
		}
		return id.toString();
	}
    
	// made by s204507
    public static boolean validName(String name) throws Exception {
		if(name.trim().length() == 0) {
			throw new Exception("Illegal Name Length 0");
		}
		return true;
    }
	
	//made by s204455
	//assuming that the active dates has ended if it ends before the available date
	public boolean isAvailable(LocalDate date) {
		int activeAtDate = activeActivitiesEndDates.size();

		for(int i = 0; i < activeActivitiesEndDates.size(); i++) {
			if (activeActivitiesEndDates.get(i).isBefore(date)) {
				activeAtDate--;
			}
		}
		
		if(activeAtDate < 20) {
			return true;
		}
		return false;
	}
	
	public Developer askForAssistance(Activity activity, List<Developer> developers) throws Exception { //app added only for testing
		List<Developer> possibleHelpers = new ArrayList<>();

		if(!activity.getAssignedDevelopers().contains(this)) {
			throw new Exception("Only ask for help on activities you are assigned to");
		}

		LocalDate now = LocalDate.now();

		for(Developer d : developers) {
			if(d.equals(this)) continue;

			if(d.isAvailable(now)) {
				possibleHelpers.add(d);
			}
		}

		if(possibleHelpers.size() > 0) {
			Developer helping = possibleHelpers.get((int) (Math.random() * (possibleHelpers.size())));
			activity.addAssistingDeveloper(helping);
			return helping;
		}
		return null;
	}
	
	public void becomeProjectManager(Project project) {
		project.setAssignedProjectManager(new ProjectManager(this));
	}

	//made by s204455
	public void addActivityEndDate(LocalDate date) {
		activeActivitiesEndDates.add(date);
	}
	
	//made by s204455
	public void removeActivityEndDate(LocalDate date) {
		//remove only one date if developer is removed from activity
		LocalDate remove = activeActivitiesEndDates.stream().filter(d -> d.toString().equals(date.toString())).findFirst().orElse(null);
		if (remove == null) { 
			System.out.println("you tried to remove an enddate from a developer in witch the enddate did not exist");
		}
		else {
			activeActivitiesEndDates.remove(remove);
		}
		updateActiveEnddates();
	}
	
	//made by s204455
	public void removeAllActivityEndDate() {
		activeActivitiesEndDates.clear();
		updateActiveEnddates();
	}
	

	//---------Getters and setters --------//

	public List<LocalDate> getAllActivityEndDate() {
		return activeActivitiesEndDates;
	}
	
	private void updateActiveEnddates() {
		this.numActiveActivities = activeActivitiesEndDates.size();
	}

	public String getID() {
		return id;
	}
	
	public void setID(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public int getNumActiveActivities() {
		return numActiveActivities;
	}

	public void setNumActiveActivities(int numActiveActivities) {
		this.numActiveActivities = numActiveActivities;
	}

	public String toString() {
		return getID() + " - " + getName();
	}
}