package dtu.application;

import java.time.LocalDate;

public class TimeRegister {
	
	//Alignment of comments: s204507 ;)
    private LocalDate date;     // Date of commit
    private int duration;		// Amount of time worked
    private Developer developer;// Who worked
    private Activity activity;	// Which activity was worked on

  //@author s204433
    // "A developer has worked on this activity for x amount of time"
    public TimeRegister(Developer developer, Activity activity, int duration) {
        this.date = LocalDate.now();
        try {
            setDuration(duration);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.developer = developer;
        this.activity = activity;
    }

    // ----- Getters and Setters ----- //

    public LocalDate getDate() { 
    	return this.date; 
    }

    public int getDuration() { 
    	return this.duration; 
    }

    public Developer getDeveloper() { 
    	return this.developer; 
    }

    public Activity getActivity() { 
    	return this.activity; 
    }

    public void setDate(LocalDate date) { 
    	this.date = date; 
    }

    public void setDuration(int duration) throws Exception {
        if(duration < 0) throw new Exception("Invalid duration");
        this.duration = duration;
    }

    public void setDeveloper(Developer developer) { 
    	this.developer = developer; 
    }

    public void setActivity(Activity activity) { 
    	this.activity = activity; 
    }
}