package dtu.application.controller;

//Main responsible: s204488

public class Target {
	private Object target;
	private String type;
	
	public Target() {
		target = null;
		type = "null";
	}
	
	public Object get() {
		return this.target;
	}
	
	public void set(Object target) {
		this.target = target;
	}
	
	public String type() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
}
