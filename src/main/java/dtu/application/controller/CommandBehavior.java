/**
 *
 * @author s204488 (Main responsible)
 * Put here and refactored by s204507
 * Made non-case-sensitive by s204507
*/

package dtu.application.controller;

import java.util.Scanner;

public class CommandBehavior {
	private static Scanner input;
	private static Directory directory;
	
	public CommandBehavior(Scanner input, Directory directory) {
		this.input = input;
		this.directory = directory;
	}

	public static void assist() {
		input.nextLine();
		
		if (isValidCommand("activity")) {
			System.out.print("Please enter a valid developer ID: ");
			String id = input.nextLine();

			System.out.print("Please enter the time to be committed: ");
			int time = Integer.parseInt(input.nextLine());
			
			directory.registerTime(id, time);
			
			directory.removeAssistingDeveloper(id);
		}
	}

	public static void get() {
		String next = input.next();
		input.nextLine();
		
		if (next.equalsIgnoreCase("assistance") && isValidCommand("activity")) {
			System.out.print("Please enter a valid developer ID: ");
			next = input.nextLine();
			
			directory.help(next);
		}
	}
	
	public static void edit() {
		String next = input.next();
		input.nextLine();
		
		if (next.equalsIgnoreCase("project") && isValidCommand("null")) {
			System.out.print("Please enter a valid project ID: ");
			String id = input.nextLine();
			
			System.out.print("Please enter a value to edit <name / description / start date / end date>: ");
			String target = input.nextLine();
			
			if (target.equals("name")) {
				System.out.print("Please enter the new name: ");
				String value = input.nextLine();
				
				directory.editProject(id, target, value);
			} else if (target.equals("description")) {
				System.out.print("Please enter the new description: ");
				String value = input.nextLine();
				
				directory.editProject(id, target, value);
			} else if (target.equals("start date")) {
				System.out.print("Please enter the new start date: ");
				String value = input.nextLine();
				
				directory.editProject(id, target, value);
			} else if (target.equals("end date")) {
				System.out.print("Please enter the new end date: ");
				String value = input.nextLine();
				
				directory.editProject(id, target, value);
			}
		} else if (next.equalsIgnoreCase("activity") && isValidCommand("project")) {
			System.out.print("Please enter a valid activity ID: ");
			String id = input.nextLine();
			
			System.out.print("Please enter a value to edit <name / description / start date / end date / expected hours>: ");
			String target = input.nextLine();
			
			String value;
			switch (target) {
			case "name":
				System.out.print("Please enter the new name: ");
				value = input.nextLine();
				
				directory.editActivity(id, target, value);
				break;
			case "description":
				System.out.print("Please enter the new description: ");
				value = input.nextLine();
				
				directory.editActivity(id, target, value);
				break;
			case "start date":
				System.out.print("Please enter the new start date: ");
				value = input.nextLine();
				
				directory.editActivity(id, target, value);
				break;
			case "end date":
				System.out.print("Please enter the new end date: ");
				value = input.nextLine();
				
				directory.editActivity(id, target, value);
				break;
			case "expected hours":
				System.out.print("Please enter the expected hours: ");
				value = input.nextLine();
				
				directory.editActivity(id, target, value);
				break;
			default:
				break;
			}
		} else if (next.equalsIgnoreCase("timeregistry")) {
			System.out.print("Please enter a valid index: ");
			int index = Integer.parseInt(input.nextLine());
			
			System.out.print("Please enter a value to edit <time / developer / date>: ");
			String target = input.nextLine();
			
			if (target.equals("time")) {
				System.out.print("Please enter a new time to be committed: ");
				String time = input.nextLine();
				directory.editTime(index, target, time);
			} else if (target.equals("developer")) {
				System.out.print("Please enter a new valid developer ID: ");
				String id = input.nextLine();
				directory.editTime(index, target, id);
			} else if (target.equals("date")) {
				System.out.print("Please enter a new valid date: ");
				String date = input.nextLine();
				directory.editTime(index, target, date);
			}
		}
	}
	
	public static void invite() {
		String next = input.next();
		input.nextLine();
		
		directory.invite(next);
	}
	
	public static void create() {
		String next = input.next();
		input.nextLine();

		if (next.equalsIgnoreCase("project") && isValidCommand("null")) {
			System.out.print("Please enter project name: ");
			String name = input.nextLine();
			
			System.out.print("Please enter project description: ");
			String desc = input.nextLine();

			directory.createProject(name, desc);
		} else if (next.equalsIgnoreCase("activity") && isValidCommand("project")) {
			System.out.print("Please enter activity name: ");
			String name = input.nextLine();
			
			System.out.print("Please enter activity description: ");
			String desc = input.nextLine();
			
			System.out.print("Please enter activity type: ");
			String type = input.nextLine();
			
			System.out.print("Please enter activity end date: ");
			String endDate = input.nextLine();
			
			directory.createActivity(name, desc, type, endDate);
		}
	}
	
	public static void remove() {
		String next = input.next();
		input.nextLine();

		if (next.equalsIgnoreCase("activity") && isValidCommand("project")) {
			System.out.println("Please enter a valid activity ID: ");
			String id = input.nextLine();
			
			directory.removeActivity(id);
		} else if (next.equalsIgnoreCase("developer")) {
			System.out.println("Please enter a valid developer ID: ");
			String id = input.nextLine();
			
			directory.removeDeveloper(id);
		} else if (next.equalsIgnoreCase("pm") && isValidCommand("project")) {
			directory.removePM();
		}
	}

	public static void view() {
		String next = input.nextLine().trim().toLowerCase();
		if (next.equalsIgnoreCase("available")) {
			System.out.print("Please enter target date: ");
			String value = input.nextLine();
			
			directory.setTarget(next, value);
		}
		directory.setTarget(next, "");
	}
	
	public static void back() {
		directory.back();
	}
	
	public static void register() {
		String next = input.next();
		input.nextLine();
		
		if (next.equalsIgnoreCase("developer")) {
			System.out.print("Please enter full name: ");
			String name = input.nextLine();
			
			directory.registerDeveloper(name);
		} else if (next.equalsIgnoreCase("pm") && isValidCommand("project")) {
			System.out.print("Please enter a valid developer id: ");
			String id = input.nextLine();
			
			directory.registerPM(id);
		} else if (next.equalsIgnoreCase("time") && isValidCommand("activity")) {
			System.out.print("Please enter a valid developer id: ");
			String id = input.nextLine();
			
			System.out.print("Please enter the time to be committed: ");
			int time = Integer.parseInt(input.nextLine());
			
			directory.registerTime(id, time);
		}
	}
	
	public static boolean isValidCommand(String command) {
		return command.equalsIgnoreCase(directory.validateTarget());
	}
}