package dtu.application.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import dtu.application.Developer;
import dtu.application.Info;
import dtu.application.Project;
import dtu.application.ui.Main;

/**
 * This class loads in data from a given file and adds it to an arraylist.
 *
 * @author s204433
 * help on no-id-duplicates by s204507
*/

public class DataLoader {
    static Scanner scanner;

    public static void loadDevelopersFrom(List<Developer> developers, String path) {
        File file = new File(path);

        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while(scanner.hasNextLine()) {
            String name = scanner.nextLine().trim();

            Developer developer = null;

            try {
                developer = new Developer(name);
                developer.setID(Main.generateDeveloperID(name));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(developer != null) {
                developers.add(developer);
            }
        }
    }

    public static void loadProjectsFrom(List<Project> projects, String path) {
        File file = new File(path);

        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while(scanner.hasNextLine()) {
            String[] parts = scanner.nextLine().split("\\|");
            String name = parts[0].trim();
            String description = parts[1].trim();

            Project project = new Project(new Info(name, description));
            project.setID(Main.generateProjectID());

            projects.add(project);
        }
    }
}