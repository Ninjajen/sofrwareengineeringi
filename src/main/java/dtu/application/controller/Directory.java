package dtu.application.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import dtu.application.Activity;
import dtu.application.ActivityType;
import dtu.application.Developer;
import dtu.application.Info;
import dtu.application.Project;
import dtu.application.ProjectManager;
import dtu.application.TimeRegister;

/**
 * @author s204488 (Main responsible)
 * PROOFREADING BY S204433
 * Put here and refactored by s204507
 * Made non-case-sensitive by s204507
 */

public class Directory {
	private List<Project> projects;
	private List<ProjectManager> projectManagers;
	private List<Developer> developers;
	private Target currentTarget;
	
	public Directory() {
		projects = new ArrayList<>();
		projectManagers = new ArrayList<>();
		developers = new ArrayList<>();
		currentTarget = new Target();
	}
	
	//-----------------CREATE-----------------//
	
	public void createProject(String name,String desc) {
		Info info = new Info(name, desc);
		Project project = new Project(info);
		projects.add(project);
	}
	
	public void createActivity(String name, String desc, String type, String endDate) {
		ActivityType activityType = null;
		for(ActivityType t : ActivityType.values()) {
			if(t.toString().equals(type.toUpperCase())) activityType = t;
		}

		if(activityType == null) activityType = ActivityType.OTHER;

		Info info = new Info(name, desc);
		Activity activity = new Activity(activityType, info);
		
		LocalDate date = LocalDate.parse(endDate);
		activity.setEndDate(date);
		
		try {
			((Project) getTarget()).addActivity(activity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//----------------REGISTER----------------//
	
	public void registerPM(String id) {
		Developer developer = developers.stream().filter(d -> d.getID().equals(id.toUpperCase())).findFirst().orElse(null);
		
		if (developer != null) {
			try {
				ProjectManager projectManager = new ProjectManager(developer);
				((Project) getTarget()).setAssignedProjectManager(projectManager);
				((Project) getTarget()).addDeveloper(developer);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Invalid developer ID");
		}
	}
	
	public void registerDeveloper(String name) {
		Developer developer = null;
		try {
			developer = new Developer(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developers.add(developer);
	}
	
	public void registerTime(String id, int time) {
		for (Project p : projects) {
			if (p.getActivityByID(((Activity) getTarget()).getID()) == ((Activity) getTarget())) {
				List<Developer> list = new ArrayList<Developer>();
				
				((Activity) getTarget()).getAssignedDevelopers().stream().filter(
						d -> d.getID().equals(id)).forEach(list::add);
				((Activity) getTarget()).getAssistingDevelopers().stream().filter(
						d -> d.getID().equals(id)).forEach(list::add);
				
				Developer dev = list.stream().filter(
						d -> d.getID().equalsIgnoreCase(id)).findFirst().orElse(null);

				TimeRegister commit = new TimeRegister(dev, ((Activity) getTarget()), time);
				
				try {
					p.registerTime(commit);
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}
		}
	}
	
	//-----------------REMOVE-----------------//

	public void removeActivity(String id) {
		try {
			((Project) getTarget()).removeActivity(((Project) getTarget()).getActivityByID(id));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void removePM() {
		try {
			((Project) getTarget()).removeAssignedProjectManager(((Project) getTarget()).getAssignedProjectManager());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void removeDeveloper(String id) {
		
		if (currentTarget.type().equalsIgnoreCase("activity")) {
			Developer target = ((Activity) getTarget()).getAssignedDevelopers().stream().filter(
					d -> d.getID().equals(id.toUpperCase())).findFirst().orElse(null);
			
			try {
				((Activity) getTarget()).removeDeveloper(target);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (currentTarget.type().equalsIgnoreCase("project")) {
			Developer target = ((Project) getTarget()).getAssignedDevelopers().stream().filter(
					d -> d.getID().equals(id.toUpperCase())).findFirst().orElse(null);
			
			try {
				((Project) getTarget()).removeDeveloper(target);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (currentTarget.type().equalsIgnoreCase("null")) {
			Developer target = developers.stream().filter(
					d -> d.getID().equalsIgnoreCase(id.toUpperCase())).findFirst().orElse(null);
			
			List<Project> list = new ArrayList<Project>();
					
			projects.stream().filter(p -> p.getAssignedDevelopers().contains(target)).forEach(list::add);
			
			for (Project project : list) {
				try {
					project.removeDeveloper(target);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void removeAssistingDeveloper(String id) {
		Developer developer = ((Activity) getTarget()).getAssistingDevelopers().stream().filter(
				d -> d.getID().equals(id)).findFirst().orElse(null);
		
		((Activity) getTarget()).getAssistingDevelopers().remove(developer);
	}
	
	//-----------------TARGET-----------------//
	
	public void setTarget(String next, String value) {		
		if (next.equalsIgnoreCase("developers")) {
			if (getTarget() instanceof Project) {
				currentTarget.set(((Project) getTarget()).getAssignedDevelopers());
				currentTarget.setType(next);
			} else {
				currentTarget.set(developers);
				currentTarget.setType(next);
			}
		} else if (getTarget() instanceof Project) {
			currentTarget.set(((Project) getTarget()).getProjectActivities().stream().filter(
					a -> a.getID().equals(next)).findFirst().orElse(null));
			
			if (getTarget() == null) {
				System.out.println("Activity does not exist");
				currentTarget.setType("null");
			} else {
				currentTarget.setType("activity");
			}
		} else if (getTarget() == null) {
			if (next.equalsIgnoreCase("available")) {
				List<Developer> list = new ArrayList<Developer>();
				
				LocalDate date = LocalDate.parse(value);
				
				developers.stream().filter(d -> d.isAvailable(date)).forEach(list::add);
				
				currentTarget.set(list);
				currentTarget.setType("developers");
			} else {
				currentTarget.set(projects.stream().filter(p -> p.getID().equals(next)).findFirst().orElse(null));
				
				if (getTarget() == null) {
					System.out.println("Project does not exist");
				} else {
					currentTarget.setType("project");
				}
			}
		} else if (next.equalsIgnoreCase("timeregistry")) {
			if (currentTarget.type() == "activity") {
				Project target = projects.stream().filter(
						p -> p.getProjectActivities().contains(((Activity) getTarget()))).findFirst().orElse(null);
				
				List<TimeRegister> list = new ArrayList<TimeRegister>();
				target.getTimeRegister().stream().filter(
						t -> t.getActivity().equals(((Activity) getTarget()))).forEach(list::add);
				currentTarget.set(list);
				
				currentTarget.setType(next);
			}
		}
	}
	
	public void back() {
		currentTarget.set(null);
		currentTarget.setType("null");
	}
	
	//------------------EDIT------------------//
	
	public void editProject(String id, String target, String value) {
		Project project = projects.stream().filter(
				p -> p.getID().equalsIgnoreCase(id)).findFirst().orElse(null);

		if (project != null) {
			if (target.equalsIgnoreCase("name")) {
				project.getInfo().setName(value);
			} else if (target.equalsIgnoreCase("description")) {
				project.getInfo().setDescription(value);
			} else if (target.equalsIgnoreCase("start date")) {
				project.setStartDate(LocalDate.parse(value));
			} else if (target.equalsIgnoreCase("end date")) {
				project.setEndDate(LocalDate.parse(value));
			}
		}
	}
	
	public void editActivity(String id, String target, String value) {
		Activity activity = ((Project) getTarget()).getProjectActivities().stream().filter(
				a -> a.getID().equalsIgnoreCase(id)).findFirst().orElse(null);
		
		if (activity != null) {
			if (target.equalsIgnoreCase("name")) {
				activity.getInfo().setName(value);
			} else if (target.equalsIgnoreCase("description")) {
				activity.getInfo().setDescription(value);
			} else if (target.equalsIgnoreCase("start date")) {
				activity.setStartDate(LocalDate.parse(value));
			} else if (target.equalsIgnoreCase("end date")) {
				activity.setEndDate(LocalDate.parse(value));
			} else if (target.equalsIgnoreCase("expected hours")) {
				try {
					activity.setExpectedHours(Integer.parseInt(value));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void editTime(int index, String target, String value) {
		List<TimeRegister> list = ((List<TimeRegister>) getTarget());
		
		if (target.equalsIgnoreCase("time")) {
			try {
				list.get(index).setDuration(Integer.parseInt(value));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (target.equalsIgnoreCase("developer")) {
			Developer developer = list.get(index).getActivity().getAssignedDevelopers().stream().filter(
					d -> d.getID().equalsIgnoreCase(value)).findFirst().orElse(null);
			
			if (developer != null) { 
				list.get(index).setDeveloper(developer);
			} else {
				System.out.println("Invalid developer ID");
			}
		} else if (target.equalsIgnoreCase("date")) {
			LocalDate date = LocalDate.parse(value);
			
			list.get(index).setDate(date);
		}
	}
	
	//-----------------INVITE-----------------//
	
	public void invite(String next) {
		if (getTarget() instanceof Project) {
			developers.stream().filter(d -> d.getID().equalsIgnoreCase(next)).forEach(d -> {
				try {
					((Project) getTarget()).addDeveloper(d);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} else if (getTarget() instanceof Activity) {
			for(Developer developer : developers) {
				if(developer.getID().equalsIgnoreCase(next.toUpperCase())) {
					try {
						((Activity) getTarget()).addDeveloper(developer);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}
	}

	//------------------HELP------------------//
	
	public void help(String next) {
		Developer developer = ((Activity) getTarget()).getAssignedDevelopers().stream().filter(
				d -> d.getID().equalsIgnoreCase(next)).findFirst().orElse(null);
		
		if (developer != null) {
			try {
				developer.askForAssistance(((Activity) getTarget()), developers);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Invalid developer ID");
		}
	}
	
	//----------GETTERS AND SETTERS-----------//
	
	public String validateTarget() {
		return currentTarget.type();
	}
	
	public Object getTarget() {
		return currentTarget.get();
	}
	
	public List<Project> getProjectList() {
		return projects;
	}
	
	public List<Developer> getDeveloperList() {
		return developers;
	}
}