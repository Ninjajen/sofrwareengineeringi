package dtu.application;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//Main responsible: s205533

public class TestApp {
    public List<Project> projects = new ArrayList<>();
    public List<Developer> developers = new ArrayList<>();
    public List<Developer> availableDevelopers = new ArrayList<>();
    public Project project;
    public Developer developer;
    public List<ProjectManager> projectManagers = new ArrayList<>();
    public ProjectManager projectManager;
    public Activity activity;
    public Info info;
    public LocalDate date;
    public String id;
    public int integerBuffer;

	public void addDeveloper(Developer dev) {
		this.developers.add(dev);
	}
}