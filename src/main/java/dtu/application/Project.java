package dtu.application;

import java.time.LocalDate;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

//Main responsible: s204433

public class Project {
    private String id;
    private Info info;
    private ProjectManager assignedProjectManager;

    private LocalDate startDate;
    private LocalDate endDate;

    private long projectDuration;
    private boolean isExternal;

    private List<TimeRegister> timeRegister;

    private List<Developer> assignedDevelopers;
    private List<Activity> projectActivities;

    public Project(Info info) {
        this.info = info;
        this.id = generateID();

        timeRegister = new ArrayList<>();

        assignedDevelopers = new ArrayList<>();
        projectActivities = new ArrayList<>();
    }
    
	// -------- ID Generation ----------- //
    // made by s204507
    private String generateID() {
        String id = "";

        // Get last two digits of the current year
        String year = Integer.toString(LocalDate.now().getYear());
        id += year.substring(year.length() - 2);

        // Generate a random 4 digit number
        Random random = new Random();
        id += String.format("%04d", random.nextInt(10000));

        return id;
    }
    
    //@author s204433
    public boolean validateUniqueActivityID(String id) {
    	return projectActivities.stream().anyMatch(a -> a.getID().equals(id));
    }

        public void registerTime(TimeRegister commit) throws Exception {
    	
    	// if project doesn't contain the activity 
        if(!getProjectActivities().contains(commit.getActivity())) {
            throw new Exception("Cannot commit time to activity that doesn't exist in project");
        // else activity must be contained in the project
        // developer can register time if assigned
        } else {
            if (commit.getActivity().getAssignedDevelopers().contains(commit.getDeveloper()) ||
            		commit.getActivity().getAssistingDevelopers().contains(commit.getDeveloper())) {
                getTimeRegister().add(commit);
            // otherwise the developer isn't assigned. 
            } else {
                throw new Exception("Developer not assigned to activity; cannot register time to activity");
            }
        }
    }

    // ----- Project ----- //

    //@author s204433
    public void updateDuration() {
        SortedSet<LocalDate> dates = new TreeSet<>();
        for(Activity activity : getProjectActivities()) {
            if(activity.getStartDate() != null) dates.add(activity.getStartDate());
            if(activity.getEndDate() != null) dates.add(activity.getEndDate());
        }

        if(dates.size() > 1) projectDuration = DAYS.between(dates.first(), dates.last());
    }


    // ----- Activities ----- //

    //s204488
    public void addActivity(Activity activity) throws Exception {
        if (assignedProjectManager != null) {
            projectActivities.add(activity);
        } else {
            throw new Exception("Only project managers can add activities");
        }
    }

    //made by s204488
    //developer handling by s204455
    public void removeActivity(Activity activity) throws Exception {
        if (assignedProjectManager != null) {
            if (projectActivities.stream().anyMatch(a -> a.equals(activity))) {
                activity.getAssignedDevelopers().forEach(d -> d.removeActivityEndDate(activity.getEndDate()));
                activity.getAssistingDevelopers().forEach(d -> d.removeActivityEndDate(activity.getEndDate()));
                projectActivities.remove(activity);
            } else {
                throw new Exception("Activity not found");
            }
        } else {
            throw new Exception("Only project managers can remove activities");
        }
    }

    // ----- ProjectManager ----- //

    //@author s204433
    public boolean authenticateProjectLeader(ProjectManager projectManager) {
        return assignedProjectManager != null && assignedProjectManager.equals(projectManager);
    }
    
    //@author s204488
    public void removeAssignedProjectManager(ProjectManager projectManager) throws Exception {
    	if (authenticateProjectLeader(projectManager)) {
    		assignedProjectManager = null;
    	} else {
    		throw new Exception("Project manager is not manager of this project");
    	}
    }

    // ----- Developer ----- //
    
    //@author s204488
    public void addDeveloper(Developer developer) throws Exception {
    	if (assignedProjectManager != null) {
    		assignedDevelopers.add(developer);
    	} else {
    		throw new Exception("Only project managers can add developers to a project");
    	}
    }
    
    //@author s204488
    public void removeDeveloper(Developer developer) throws Exception {
    	if (assignedProjectManager != null) {
    		for (Activity a : this.getProjectActivities()) {
    			if(a.getAssignedDevelopers().contains(developer)) {
    				a.removeDeveloper(developer);
    			}
    		}
    		assignedDevelopers.remove(developer);
    	} else {
    		throw new Exception("Only project managers can remove developers from a project");
    	}
    }

    // ----- Getters and Setters ----- //
    
    public List<Developer> getAssignedDevelopers() {
    	return assignedDevelopers;
    }
    
    public long getExpectedHours() {
        return getProjectActivities().stream().mapToLong(Activity::getExpectedHours).sum();
    }
    
    public ProjectManager getAssignedProjectManager() {
    	return assignedProjectManager;
    }
    
    public void setAssignedProjectManager(ProjectManager newProjectManager) {
    	this.assignedProjectManager = newProjectManager;
    }

    public String getID() {
    	return this.id;
    }

    public void setID(String id) { this.id = id; }
    
    public Activity getActivityByID(String id) {
    	return projectActivities.stream().filter(a -> a.getID().equals(id)).findAny().orElse(null);
    }
    
    public List<Activity> getProjectActivities() {
    	return projectActivities;
    }

    public void setStartDate(LocalDate date) {
    	this.startDate = date;
    }
    
    public LocalDate getStartDate() {
    	return this.startDate;
    }
    
    public void setEndDate(LocalDate date) {
    	this.endDate = date;
    }
    
    public LocalDate getEndDate() {
    	return this.endDate;
    }

    public void setProjectDuration(long projectDuration) {
        this.projectDuration = projectDuration;
    }

    public long getProjectDuration() {
    	// Notice: This may not be the correct duration
    	return projectDuration;
    }
    
    public List<TimeRegister> getTimeRegister() {
    	return this.timeRegister;
    }
    
    public Info getInfo() {
    	return this.info;
    }
}
