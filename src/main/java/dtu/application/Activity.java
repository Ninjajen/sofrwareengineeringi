package dtu.application;

import java.time.LocalDate;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;


// Main responsible s204455

public class Activity {

	private String id; 

	private Info info;
	private ActivityType type;

	private LocalDate startDate = null;
	private LocalDate endDate = null;

	private double totalTimeCommitted;
	private long expectedHours;

	private List<Developer> assignedDevelopers;
	private List<Developer> assistingDevelopers;

	public Activity(ActivityType type, Info info) {
		this.type = type;
		this.info = info;

		this.id = generateID();

		this.assignedDevelopers = new ArrayList<>();
		this.assistingDevelopers = new ArrayList<>();
	}
	
	// ----- ID Generation ----- //
	
    //@author s204507
    private String generateID() {
		// Generate a random 6 digit number
		Random random = new Random();
		return String.format("%04d", random.nextInt(1000000));
    }

    //@author s204455
	public void addDeveloper(Developer developer) throws Exception {
		if(!assignedDevelopers.contains(developer)) {
			assignedDevelopers.add(developer);
			developer.setNumActiveActivities(developer.getNumActiveActivities() + 1);
			developer.addActivityEndDate(getEndDate());
		} else {
			throw new Exception("Developer already assigned to activity");
		}
	}

    //@author s204455
	public void removeDeveloper(Developer developer) throws Exception {
		if(assignedDevelopers.contains(developer)) {
			assignedDevelopers.remove(developer);
			developer.setNumActiveActivities(developer.getNumActiveActivities() - 1);
		} else {
			throw new Exception("This developer is not assigned to the activity");
		}
	}
	
    //@author s204455
	public void addAssistingDeveloper(Developer developer) {
		this.assistingDevelopers.add(developer);
	}
	
	// ----- Getters and Setters ----- //
			
	public List<Developer> getAssistingDevelopers() {
		return this.assistingDevelopers;
	}
	
	public List<Developer> getAssignedDevelopers() {
		return this.assignedDevelopers;
	}

	public void setStartDate(LocalDate date) {
		this.startDate = date;
	}

	public LocalDate getStartDate() {
		return this.startDate;
	}

	public void setEndDate(LocalDate date) {
		this.endDate = date;
	}

	public LocalDate getEndDate() {
		return this.endDate;
	}

	public long getDuration() {
		if(startDate != null && endDate != null) {
			return DAYS.between(startDate, endDate);
		}

		return 0;
	}

	public void setExpectedHours(long hours) throws Exception {
		if(hours < 0) throw new Exception("Expected number of work hours cannot be lower than 0");
		this.expectedHours = hours;
	}
	
	public long getExpectedHours() {
		return expectedHours;
	}

	public String getID() {
		return id;
	}

	public Info getInfo() {
		return info;
	}

	public ActivityType getType() {
		return type;
	}
	
	public String toString() {
		String infoString;

		if(info != null) {
			if(!info.getName().equals("")) {
				infoString = info.getName();
			} else {
				infoString = "Unnamed Activity";
			}

			if(!info.getDescription().equals("")) {
				infoString += " : " + info.getDescription();
			} else {
				infoString += " : No Description";
			}
		} else {
			infoString = "Unnamed Activity : No Description";
		}

		return infoString;
	}
}