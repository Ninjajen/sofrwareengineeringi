// Made by Jenifer, s204507

package dtu.application;

public enum ActivityType {
    DEMANDS, PLANNING, ANALYSIS, DESIGN, PROGRAMMING, REFACTORING, TEST, PERSONAL, COURSES, OTHER
}