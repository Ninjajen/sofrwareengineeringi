/**
 *
 * @author s204488 (Main responsible)
 * Put here and refactored by s204507
*/

package dtu.application.ui;

public class Commands {
	
	public static String[] directoryCommands = {"-create <project>",
			"-register <developer>",
			"-edit <project>",
			"-view <ID / developers / available>"};

	public static String[] projectCommands   = {"-create <activity>",
		    "-register <pm>",
		    "-invite <ID>",
		    "-edit <activity>",
		    "-remove <activity / developer / pm>",
		    "-view <developers / ID>",
		    "-back"};

	public static String[] activityCommands  = {"-invite <ID>",
			"-register <time>",
			"-view <timeregistry>",
			"-remove <developer>",
			"-get assistance",
			"-assist",
			"-back"};

	public static String[] developerCommands = {"-back"};

	public static String[] registryCommands =  {"-edit <timeregistry>",
			"-back"};

}
