
/**
 * @author s204488
 * REFACTORED BY s204507
 * Made non-case-sensitive by s204507
*/

package dtu.application.ui;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import dtu.application.Activity;
import dtu.application.Developer;
import dtu.application.Project;
import dtu.application.TimeRegister;
import dtu.application.controller.CommandBehavior;
import dtu.application.controller.DataLoader;
import dtu.application.controller.Directory;

public class Main {
	private static Scanner input;
	private static Directory directory;
	private static String[] query;
	private static CommandBehavior commandbehavior;

	public static void main(String[] args) {
		directory = new Directory();

		// Load empty Developers & Projects into directory (For testing) - S204433
		DataLoader.loadDevelopersFrom(directory.getDeveloperList(), "res/developers.txt");
		DataLoader.loadProjectsFrom(directory.getProjectList(),"res/projects.txt");

		input = new Scanner(System.in);

		CommandBehavior commandbehavior = new CommandBehavior(input, directory);
		
		printUI();
		
		while(true) {
			switch (input.next().toLowerCase()) {
			case "-create":
				CommandBehavior.create();
				break;
			case "-remove":
				CommandBehavior.remove();
				break;
			case "-view":
				CommandBehavior.view();
				break;
			case "-back":
				CommandBehavior.back();
				break;
			case "-register":
				CommandBehavior.register();
				break;
			case "-invite":
				CommandBehavior.invite();
				break;
			case "-edit":
				CommandBehavior.edit();
				break;
			case "-get":
				CommandBehavior.get();
				break;
			case "-assist":
				CommandBehavior.assist();
				break;
			default:
				System.out.println("Unknown command");
				break;
			}
			printUI();
		}
	}
	
	public static void printUI() {
		if (CommandBehavior.isValidCommand("null")) {
			System.out.println("|---------------------------------Directory---------------------------------|");
			for (Project project : directory.getProjectList()) {
				System.out.println("Project " + "(" + project.getID() + "): " + project.getInfo().toString());
			}
		} else if (CommandBehavior.isValidCommand("project")) {
			System.out.println("|---------------------------------(" + ((Project) directory.getTarget()).getID() + ")---------------------------------|");
			
			int num = 0;
			for (TimeRegister time : ((Project) directory.getTarget()).getTimeRegister()) {
				num += time.getDuration();
			}
			
			System.out.println("Total time committed: " + num);
			
			if (((Project) directory.getTarget()).getAssignedProjectManager() != null) {
				System.out.println("Project Manager: " + ((Project) directory.getTarget()).getAssignedProjectManager().getID());
			}
			
			System.out.println();
			
			((Project) directory.getTarget()).getProjectActivities().forEach(e -> System.out.println(e.getID() + " : " + e.toString()));
		} else if (CommandBehavior.isValidCommand("developers")) {
			System.out.println("|---------------------------------Developers---------------------------------|");
			for (Developer dev : ((List<Developer>) directory.getTarget())) {
				System.out.print(dev.getID() + " : " + dev.getName());
				
				if (directory.getProjectList().stream().anyMatch(p -> p.getAssignedProjectManager() != null &&
						p.getAssignedProjectManager().getID().equals(dev.getID()))) {
					System.out.println(" " + "(Project Manager)");
				} else {
					System.out.println();
				}
			}
		} else if (CommandBehavior.isValidCommand("activity")) {
			System.out.println("|---------------------------------" + ((Activity) directory.getTarget()).toString() +"---------------------------------|");
			System.out.println(((Activity) directory.getTarget()).getType());
			
			System.out.println();
			
			System.out.println("Assisting developers: ");
			for (Developer dev : ((Activity) directory.getTarget()).getAssistingDevelopers()) {
				System.out.println("   " + dev.getID() + " : " + dev.getName());
			}
			
			System.out.println();
			
			System.out.println("Developers: ");
			for (Developer dev : ((Activity) directory.getTarget()).getAssignedDevelopers()) {
				System.out.println("   " + dev.getID() + " : " + dev.getName());
			}
		} else {
			System.out.println("|---------------------------------Time Registry---------------------------------|");
			List<TimeRegister> list = ((List<TimeRegister>) directory.getTarget());
			
			for (int i = 0; i < list.size(); i++) {
				System.out.println("[" + i + "] " +
								   list.get(i).getDeveloper().getID() + 
								   " : " + 
								   list.get(i).getDuration() + 
								   " : " + 
								   list.get(i).getDate());
			}
		}
		printQuery();
	}
	
	public static void printQuery() {
		switch(directory.validateTarget()) {
		case "null":
			query = Commands.directoryCommands;
			break;
		case "project":
			query = Commands.projectCommands;
			break;
		case "activity":
			query = Commands.activityCommands;
			break;
		case "developers":
			query = Commands.developerCommands;
			break;
		case "timeregistry":
			query = Commands.registryCommands;
			break;
		}
		
		System.out.println("\nAvailable Commands");
		
		for (int i = 0; i < query.length; i++) {
			System.out.println("[" + i + "] " + query[i]);
		}
	}

	//-----------------GENERATE ID-----------------//

	public static String generateDeveloperID(String name) {
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
		Random random = new Random();

		StringBuilder id = new StringBuilder();

		// split name after whitespace
		String[] parts = name.split("\\s+");

		// first char in each name
		for(int i = 0; i < parts.length && i < 4; i++) {
			id.append(parts[i].charAt(0));
		}

		// if id length != 4, add random char until length = 4
		while(id.length() < 4) {
			id.append(alphabet.charAt(random.nextInt(alphabet.length())));
		}

		// if the id is not unique, create a randomly generated id
		while(!isValidDeveloperID(directory.getDeveloperList(), id.toString())) {
			id = new StringBuilder();

			for(int i = 0; i < 4; i++) {
				id.append(alphabet.charAt(random.nextInt(alphabet.length())));
			}
		}

		return id.toString();
	}

	public static boolean isValidDeveloperID(List<Developer> developers, String ID) {
		if(ID.length() != 4) return false;
		return developers.stream().noneMatch(d -> d.getID().equals(ID));
	}

	public static String generateProjectID() {
		// Get last two digits of the current year
		String year = Integer.toString(LocalDate.now().getYear());
		year = year.substring(year.length() - 2);

		String id = "";

		while(!isValidProjectID(directory.getProjectList(), id)) {
			id = year;

			// Generate a random 4 digit number
			Random random = new Random();
			id += String.format("%04d", random.nextInt(10000));
		}

		return id;
	}

	public static boolean isValidProjectID(List<Project> projects, String ID) {
		if(ID.length() != 6) return false;
		return projects.stream().noneMatch(d -> d.getID().equals(ID));
	}
}