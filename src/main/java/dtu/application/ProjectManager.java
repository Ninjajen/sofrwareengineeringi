package dtu.application;
import java.time.LocalDate;
import java.util.*;

public class ProjectManager {
	
	private String id;
	
	public ProjectManager(Developer developer) {
		this.id = developer.getID();
	}
	
	public List<Developer> SeeAvailability(List<Developer> developers, LocalDate date){
		List<Developer> available = new ArrayList<>();
		for (Developer d : developers) {
			if(d.isAvailable(date)) {
				available.add(d);
			}
		}
		return available;
	}
	
	public void addDeveloperToActivity(Developer developer, Activity activity) throws Exception {
		if (!activity.getAssignedDevelopers().contains(developer)) {
			activity.addDeveloper(developer);
		} else {
			throw new Exception("Developer already assigned to activity");
		}
	}

	public void forfeitManagerStatus(Project project) throws Exception {
		project.removeAssignedProjectManager(this);
	}
	
	// ----- Getter(s and Setters) ----- //
	
	public String getID() {
		return id;
	}
	
	public String toString() {
		return "";
	}
}