package dtu.application;

import java.text.SimpleDateFormat;
import java.util.*;

//All made by s204455
public class Date {

	private int day;
	private int week;
	private int month;
	private int year;

	public Date(int day, int month, int week) {
		//Getting the current year
		Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		int year = Integer.parseInt(formatter.format(now.getTime()).toString().substring(6,10));
		//Setting up date calender
		this.day = day;
		this.month = month;
		this.year = year;
		//The week of the year is for some reason dislocated by exactly 4 weeks. This we fix here
		this.week = week;
	}
	
	public Date(int day, int month, int year, int week) {
		this.day = day;
		this.month = month;
		this.year = year;
		this.week = week;
	}
	
	public int timeBetweenDates(Date date) {
		int diff = 0;
		//sometimes the first week days of the next year has the last week number of the former year. This is not compatible and need to be changed
		if(this.getMonth()==1 && this.getWeek()>=52) {
			this.setWeek(0);
		}
		if(date.getMonth()==1 && date.getWeek()>=52) {
			date.setWeek(0);
		}
		//When dates are in the same year it is just the difference of week number
		if(this.getYear()==date.getYear()) {
			if(this.getWeek()>date.getWeek()) {
				return this.getWeek()-date.getWeek();
			} else {
				return date.getWeek()-this.getWeek();
			}
		}
		//when the years are different it depends on the difference
		if(this.getYear()<date.getYear()) {
			if(this.getWeek()>date.getWeek()) {
				diff += (date.getYear()-this.getYear()-1)*52;	//if more than one year apart these weeks are added
				diff += (52-this.getWeek()); 					//The rest of the week up to the next year is added
				diff += (date.getWeek()); 						//The rest of the week up to the late week is added
				return diff;
			} else { 											//when the larger year also has the larger week there has been at least a hole year between
				diff += (date.getYear()-this.getYear())*52; 	//adding the hole years
				diff += (date.getWeek()-this.getWeek()); 		//adding the remaining weeks
				return diff;
			}
		} else { //Otherwise the opisit is true
			return date.timeBetweenDates(this);
		}
	}
	
	public String toString() {
		String date = "";
		if(day<10) {
			date+="0" + day + "-";
		} else {
			date+= day + "-";
		}
		if(month<10) {
			date+="0" + month + "-";
		} else {
			date+= month + "-";
		}
		date+=year;
		return date.toLowerCase(); //placeholder for real value
	}
	
	public boolean isBefore(Date date) {
		if(date.getYear()>this.getYear()) {
			return true;
		} else if (date.getYear()<this.getYear()) {
			return false;
		} else if (date.getMonth()>this.getMonth()) {
			return true;
		} else if (date.getMonth()<this.getMonth()) {
			return false;
		} else if (date.getDay()>=this.getDay()) {
			return true;
		} else {
			return false;
		}
	}
	
	// ---- getters and setters ---- //
	
	public int getDay() {
		return this.day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}
	
	public int getWeek() {
		return this.week;
	}
	
	public void setWeek(int week) {
		this.week = week;
	}
	
	public int getMonth() {
		return this.month;
	}
	
	public void setMonth(int month) {
		this.month = month;
	}
	
	public int getYear() {
		return this.year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public static Date getNow() {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		int year = Integer.parseInt(formatter.format(now.getTime()).toString().substring(6,10));
		int month = Integer.parseInt(formatter.format(now.getTime()).toString().substring(3,5));
		int day = Integer.parseInt(formatter.format(now.getTime()).toString().substring(0,2));
		return new Date(day, month, year);
	}
}