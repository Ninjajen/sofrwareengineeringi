#Made by s204507
Feature: Remove Activity
    Description: An activity is removed
    Actors: Project manager

Scenario: Remove an existing activity
    Given there is a project
    And the standard developers are in working at the company
    And the project has a project manager
    And the project has an activity named "Activity Test" and type "TEST"
    And developer "ADEV" is assigned to the activity
    When the project manager removes the activity "Activity Test"
    Then the activity "Activity Test" is removed from the project
    And developer "ADEV" do not have the activity active

Scenario: Removing an activity when not project manager
    Given there is a project
    And that a developer is assigned to that project
    And the project has an activity named "Activity Test" and type "TEST"
    When the developer removes the activity
    Then the error message "Only project managers can remove activities" is given

#Made by s204455
Scenario: Removing the project activity removes the date from developer
    Given there is a project
    And that a developer is assigned to that project
    And the project has an activity named "Activity Test" and type "TEST"
	And the activity has a developer
    When the project manager removes the activity "Activity Test"
    Then the developer has no active activities or activity end dates

Scenario: Removing the activity when another activity with the same date is in developer only one date is removed
    Given there is a project
    And the standard developers are in working at the company
    And the project has a project manager
    And the developer "ADEV" is assigned to that project
    And the project has an activity named "Activity Test" and type "TEST"
    And developer "ADEV" is assigned to the activity
	And the activity has a developer who is assigned to other activities with the same end date
    When the project manager removes the activity "Activity Test"
    Then the developer has an activity with this date
	And the developer has one less active activity