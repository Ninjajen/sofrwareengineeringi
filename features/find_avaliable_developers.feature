#Made by s204455
Feature: Find Available Developers
    Description: The Project Manager searches for available developers for the project
    Actor: Project Manager

Scenario: The project manager searches successfully for available developers
    Given there is a project
	And the standard developers are in working at the company
    And there is a project manager
    And "ADEV" and "CHEF" are available developers
    When the project manager searches for available developers
    Then a list of available developers, containing "ADEV" and "CHEF" is returned

Scenario: The project manager searches unsuccessfully for available developers
    Given there is a project
	And the standard developers are in working at the company
    And there is a project manager
    And there are no available developers
    When the project manager searches for available developers
    Then an empty list is returned

Scenario: The project manager adds a developer to the project
    Given there is a project
	And the standard developers are in working at the company
    And there is a project manager
    And all developers are available
    When the project manager searches for available developers
    And the project manager adds the developer "BOSS" from the list of available developers to the project 
    Then the developer is added to the project

