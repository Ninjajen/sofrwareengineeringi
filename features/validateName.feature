#Made by s204488
Feature: Validate Name
    Description: Validates developer name.
    Actors: System

Scenario: Empty Name
    Given there is a name ""
    When the name is validated
    Then the error message "Illegal Name Length 0" is given

Scenario: Invalid Name
    Given there is a name "   "
    When the name is validated
    Then the error message "Illegal Name Length 0" is given

Scenario: Valid Name
    Given there is a name "John Doe"
    When the name is validated
    Then the method returns true