#Made by s204488
Feature: Create Activity
    Description: An activity is created.
    Actors: Project manager

Scenario: Create an activity successfully
    Given there is a project
    And there is a project manager
    When an activity is created within the project
    Then the activity exists in the project
    And the activityID is unique within the project
	
Scenario: A developer creates an activity
    Given there is a project
    And the project has a developer
    And there is another developer who is not the project manager
    When the developer creates an activity
    Then the error message "Only project managers can add activities" is given
#There was supposed to be more, most likely overwritten by git. Made by s204455