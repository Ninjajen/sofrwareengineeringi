#Made by s204433
Feature: Project Scheduling
    Description: The Project Manager sets and changes the schedule of the project activities
    Actor: Project Manager

Scenario: Set the first activity schedule
    Given there is a project
    And there is a project manager
    And there are no activities in the project
    When the project manager creates an activity with start date "12-02-2021" and end date "13-02-2021"
    Then the project duration is 1

Scenario: Change activity schedule
    Given there is a project
    And there is a project manager
    And the project manager creates an activity with start date "12-02-2021" and end date "13-02-2021"
    When the project manager changes the end date of the activity to "14-02-2021"
    Then the project duration is 2

Scenario: Set multiple activity schedule
    Given there is a project
    And there is a project manager
    And the project manager creates an activity with start date "12-02-2021" and end date "13-02-2021"
    When the project manager creates an activity with start date "14-02-2021" and end date "15-02-2021"
    Then the project duration is 3

Scenario: Set project start date
    Given there is a developer
    When the developer creates a project with start date "15-02-2021"
    Then the project start date is "15-02-2021"

Scenario: Set project end date
    Given there is a project
    And there is a project manager
    When the project manager sets the end date to "17-02-2021"
    Then the project end date is "17-02-2021"

Scenario: Change project deadline
    Given there is a project with a end date "17-02-2021"
    And there is a project manager
    When the project manager changes the end date to "19-02-2021"
    Then the project end date is "19-02-2021"

Scenario: Set a project activity start and end date
    Given there is a project
    And there is a project manager
    And an activity is created within the project
    When the project manager sets the activity start date to "14-02-2021" and the end date to "19-02-2021"
    Then the activity duration is 5

Scenario: Set the project activity expected hours
    Given there is a project
    And there is a project manager
    And an activity is created within the project
    When the project manager sets the activity's expected work hours to 50
    Then the project has 50 hours of expected work