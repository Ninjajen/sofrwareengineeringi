#Made by s204455
Feature: Ask For Assistance
    Description: An assigned developer asks for assistance of all other developers
    Actors: Developer
    
Scenario: The developer asks for help when only one available developer
	Given the standard developers are in working at the company
    
Scenario: The developer asks for help when only one available developer
    Given there is a project
    And the standard developers are in working at the company
    And the project has an activity named "Activity Test" and type "TEST"
    And developer "ADEV" is assigned to the activity
    And developer "HELP" is available
    And no other developers than "HELP" are available
    When the assigned developer "ADEV" asks for assistance on the activity
    Then the developer "HELP" assists the assigned developer "ADEV" on the activity 
	#for now just that Help assist on the activity

Scenario: No developer to get help from
    Given there is a project
    And the standard developers are in working at the company
    And the project has an activity named "Activity Test" and type "TEST"
    And developer "ADEV" is assigned to the activity
    And no other developers than "ADEV" are available
    When the assigned developer "ADEV" asks for assistance on the activity
    Then the assigned developer receives no help on the activity
    
Scenario: The developer asks for help on the wrong activity
    Given there is a project
    And the standard developers are in working at the company
    And the project has an activity named "Activity Test" and type "TEST"
    And the project has an activity named "Activity Test2" and type "TEST"
    And the developer "ADEV" is assigned to the activity "Activity Test"
    When the assigned developer "ADEV" asks for assistance on activity "Activity Test2"
    Then the error message "Only ask for help on activities you are assigned to" is given
    
Scenario: The developer asks for help when one or more developers are available
    Given there is a project
    And the standard developers are in working at the company
    And the project has an activity named "Activity Test" and type "TEST"
    And developer "ADEV" is assigned to the activity
    And developer "HELP" is available
    And developer "BOSS" is available
    And no other developers than "HELP" and "BOSS" are available
    When the assigned developer "ADEV" asks for assistance on the activity
    Then the assigned developer receives assistance from either the developer "HELP" or the developer "BOSS"