# Made by s204507
Feature: Create developer ID
	Description: A developer is assigned a unique dev-ID within the company
	Actor: Developer, System 

Scenario: Developer with no name
	Given there is a person with the name " " 
	Then the error message "Illegal Name Length 0" is given
	
Scenario: Developer with four or less unique initials
	Given there is a person with the name "Jane Doe" 
	And there exists no other developers with the ID "JD"
	When the person is added to the company system
	Then the developer is assigned a unique ID 
	And the first chars are the initials "JD"
	And the person is in the company system
	
Scenario: Developer with more than four unique initials
	Given there is a person with the name "Jane First Second Third Doe"
	And there exists no other developers with the ID "JFST"
	When the person is added to the company system
	Then the developer is assigned a unique ID "JFST"
	
Scenario: Developer with initials already in the system
	Given there is a person with the name "John Doe"
	And there already exists a developer with the Initials "JD"
	When the person is added to the company system
	Then the developer is assigned a unique ID of four characters
	
Scenario: WBT Developer with one initial
	Given there is a person with the name "Jane" 
	And there exists no other developers with the ID "J"
	When the person is added to the company system
	Then the developer is assigned a unique ID 
	And the first chars are the initials "J"
	And the person is in the company system

Scenario: WBT Developer with three initials
	Given there is a person with the name "Jane Something Doe" 
	And there exists no other developers with the ID "JSD"
	When the person is added to the company system
	Then the developer is assigned a unique ID 
	And the first chars are the initials "JSD"
	And the person is in the company system