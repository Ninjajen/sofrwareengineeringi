#Made by s204488
Feature: Activity toString
    Description: A white-box test of the activity toString method
    Actors: Developer
    
Scenario: There is an activity with name and description
   Given the activity has the name "Test" and description "Test Activity"
   When the toString is found
   Then the description is "Test : Test Activity"
   
Scenario: There is an activity with no name and description
   Given the activity has the name "" and description "Test Activity"
   When the toString is found
   Then the description is "Unnamed Activity : Test Activity"
   
Scenario: There is an activity with name and no description
   Given the activity has the name "Test" and description ""
   When the toString is found
   Then the description is "Test : No Description"
   
Scenario: There is an activity with no name and no description
   Given the activity has the name "" and description ""
   When the toString is found
   Then the description is "Unnamed Activity : No Description"
   
Scenario: There is an activity with no info
   Given the info is not there
   When the toString is found
   Then the description is "Unnamed Activity : No Description"