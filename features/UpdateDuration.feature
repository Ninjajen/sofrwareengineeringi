Feature: Update Duration
    Description: Validates updateDuration()
    Actors: Project manager

Background:
    Given there is a project
    And the project has a project manager

Scenario: No activities
    Given the project has no activities
    When the project duration is updated
    Then the project duration is 0

Scenario: Activities with no start dates or end dates
    Given the project has some activities with no start- or end-dates
    When the project duration is updated
    Then the project duration is 0

Scenario: Activities with both start dates and end dates
    Given the project has an activity with start date "2021-05-17" and end date "2021-06-20"
    And the project has an activity with start date "2021-07-10" and end date "2021-07-15"
    And the project has an activity with start date "2021-04-05" and end date "2021-06-28"
    When the project duration is updated
    Then the project duration is 101

Scenario: Activities with only start dates
    Given the project has an activity with start date "2021-02-17"
    And the project has an activity with start date "2021-07-10"
    And the project has an activity with start date "2021-05-05"
    When the project duration is updated
    Then the project duration is 143

Scenario: Activities with only end dates
    Given the project has an activity with end date "2021-03-27"
    And the project has an activity with end date "2021-05-03"
    And the project has an activity with end date "2021-08-11"
    When the project duration is updated
    Then the project duration is 137