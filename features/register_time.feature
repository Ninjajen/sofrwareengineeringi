#Made by s204433
Feature: Register Time
    Description: A developer registers time worked on an activity
    Actors: Developer

Background: The system has a project and activity
    Given there is a project
    And the project has a project manager
    And the project has an activity

Scenario: Register time successfully
    Given the activity has a developer
    And the developer has no previous time registered
    And the activity has no previous time registered
    When the developer registers 4 hours worked on the activity
    Then the developer has a registered time of 4 hours
    And the activity has a registered time of 4 hours

Scenario: Developer not assigned to activity
    Given the activity has no developers
    And there is a developer
    And the developer is not assigned to the activity
    When the developer registers 4 hours worked on the activity
    Then the error message "Developer not assigned to activity; cannot register time to activity" is given

Scenario: Activity is not in the project
    Given there is an activity which is not a part of the project
    And there is a developer
    When the developer registers 4 hours worked on the activity
    Then the error message "Cannot commit time to activity that doesn't exist in project" is given

Scenario: Edit time successfully
    Given the activity has a developer
    When the developer registers 4 hours worked on the activity
    Then the developer has a registered time of 4 hours
    And the activity has a registered time of 4 hours
    When the developer edits the registered time by adding 2 hours worked on the activity
    Then the developer has a registered time of 6 hours
    And the activity has a registered time of 6 hours

Scenario: Invalid time edit
    Given the activity has a developer
    When the developer registers 4 hours worked on the activity
    Then the developer has a registered time of 4 hours
    And the activity has a registered time of 4 hours
    When the developer edits the registered time by adding -6 hours worked on the activity
    Then the error message "Invalid duration" is given