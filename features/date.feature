#All Written by s204455
Feature: Date Test
    Description: Dates are created and compared
    Actors: System

Scenario: Date is created with day and month
    When a date is created with day 5 and month 7
 	Then the date is at the 5 day and the 7 month
    And the date has the toString "05-07-2021"

Scenario: Date is created with day, month and year
    When a date is created with day 5 and month 7 and the year 2022
   	Then the date is at the 5 day and the 7 month and the 2022 year
    And the date has the toString "05-07-2022"

Scenario: Date is created with day, month and year, edge case
    When a date is created with day 31 and month 12 and the year 2028
   	Then the date is at the 31 day and the 12 month and the 2028 year

Scenario: Two dates with day and month is compared
    Given there is a date with day 5 and month 7
    And there is another date with day 15 and month 8
    When the two dates are compared
    Then the date difference is 41

Scenario: Two dates with day, month and year is compared
    Given there is a date with day 5 and month 7 and year 2021
    And there is another date with day 15 and month 8 and year 2022
    When the two dates are compared
    Then the date difference is 406

Scenario: Two dates, one with day and month and one with day, month and year is compared
    Given there is a date with day 5 and month 7 and year 2023
    And there is another date with day 15 and month 8
    When the two dates are compared
    Then the date difference is -689

Scenario: Two dates are compared, week number edge case
    Given there is a date with day 27 and month 10 and year 2021
    And there is another date with day 1 and month 1 and year 2023
    When the two dates are compared
    Then the date difference is 431

Scenario: Two dates are compared, week number edge case other way around
    Given there is a date with day 1 and month 1 and year 2023
    And there is another date with day 27 and month 10 and year 2021
    When the two dates are compared
    Then the date difference is -431

Scenario: Two dates are compared, week number edge case
    Given there is a date with day 27 and month 10 and year 2021
    And there is another date with day 1 and month 1 and year 2021
    When the two dates are compared
    Then the date difference is -299

Scenario: Two dates are compared, week number edge case
    Given there is a date with day 27 and month 10 and year 2028
    And there is another date with day 3 and month 2 and year 2021
    When the two dates are compared
    Then the date difference is -2823

# Changing date info

Scenario: Date is created with day and month and changed
    Given there is a date with day 5 and month 7
    When the date is changed to the 19 day and the 8 month
 	Then the date is at the 19 day and the 8 month
    And the date has the toString "19-08-2021"

Scenario: Date is created with day, month and year and changed
	Given there is a date with day 5 and month 7 and year 2023
    When the date is changed to the 19 day and the 12 month and year 2027
 	Then the date is at the 19 day and the 12 month
    And the date has the toString "19-12-2027"

#-------------------------------------------Scenarios for no week input-------------------------------------------------
#Scenario: Date is created with day and month
#    When a date is created with day 5 and month 7
# 	Then the date is at the 5 day and the 7 month and the 27 week
#    And the date has the toString "05-07-2021"
#
#Scenario: Date is created with day, month and year
#    When a date is created with day 5 and month 7 and the year 2022
#   	Then the date is at the 5 day and the 7 month and the 2022 year and the 27 week
#    And the date has the toString "05-07-2022"
#
#Scenario: Date is created with day, month and year, exstra
#    When a date is created with day 31 and month 12 and the year 2028
#   	Then the date is at the 31 day and the 12 month and the 2028 year and the 52 week
#
#Scenario: Two dates with day and month is compared
#    Given there is a date with day 5 and month 7
#    And there is another date with day 15 and month 8
#    When the two dates are compared
#    Then the date difference is 5
#
#Scenario: Two dates with day, month and year is compared
#    Given there is a date with day 5 and month 7 and year 2021
#    And there is another date with day 15 and month 8 and year 2022
#    When the two dates are compared
#    Then the date difference is 57
#
#Scenario: Two dates, one with day and month and one with day, month and year is compared
#    Given there is a date with day 5 and month 7 and year 2023
#    And there is another date with day 15 and month 8
#    When the two dates are compared
#    Then the date difference is 99
#
#Scenario: Two dates with day, month and year is compared and first is much larger
#    Given there is a date with day 27 and month 10 and year 2021
#    And there is another date with day 1 and month 1 and year 2023
#    When the two dates are compared
#    Then the date difference is 61