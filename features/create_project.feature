#Made by s204433
Feature: Create a project
    Description: Managing projects by creating, editing, and removing them.
    Actor: Developer

Scenario: Create a project
    Given there is a developer
    When the developer creates a new project "new project"
    Then the project "new project" is created
    And the project has a project ID

Scenario: Remove a project
    Given there is a project "Test"
    When the project manager removes "Test"
    Then the project "Test" is deleted

Scenario: Developer assigns himself as project manager
    Given there is a project "Test"
    And there is a developer
    And the project has no assigned project manager
    When the developer assigns himself as the project manager
    Then the developer is assigned as the project manager

Scenario: Change project name and description
    Given there is a project "Project Name"
    And there is a project manager
    When the project manager changes the project name to "New Project Name" and the description to "New Project Description"
    Then the project name is "New Project Name" and the description is "New Project Description"

#made by Jonathan
Scenario: A developer is trying to get assigned to a project with no manager
    Given there is a project "Test"
    And there is a developer
    And the project has no assigned project manager
    When the developer gets assigned to the project
    Then the error message "Only project managers can add developers to a project" is given
    
Scenario: A developer gets removed from the project
    Given there is a project "Test"
    And there is a project manager
    And there is a developer
    And the developer is assigned to the project
    When the project manager removes the developer
    Then the developer is no longer in the project

Scenario: A developer tries to get removed from a project with no project manager
    Given there is a project "Test"
    And there is a project manager
    And there is a developer
    And the developer is assigned to the project
    And the project manager is removed
    When the project manager removes the developer
    Then the error message "Only project managers can remove developers from a project" is given
