#originally made by 204433
# completely rewritten by s204507
Feature: Add Developer to Activity
    Description: Adding a developer to a project activity
    Actor: Developer, Project Manager

Scenario: Project Manager adds a developer to an activity
	Given there is a project
    And there is a project manager
    And there is an activity within the project
    And there are no other developers assigned to the activity
    When the project manager assigns the developer "ADEV"
    Then the developer is added to the project activity
    
Scenario: Project Manger adds a developer already assigned to activity
	Given there is a project
    And there is a project manager
    And there is an activity within the project
	And the developer "ADEV" already is assigned to the activity
	When the project manager assigns the developer "ADEV"
    Then the error message "Developer already assigned to activity" is given

Scenario: Project Manager removes a developer assigned to the activity from the activity
	Given there is a project
    And there is a project manager
    And there is an activity within the project
    And the developer "ADEV" already is assigned to the activity
    When the project manager removes the developer "ADEV" from the activity
    Then the developer is removed from the project activity
    
Scenario: Project Manager removes a developer not assigned to the activity
	Given there is a project
    And there is a project manager
    And there is an activity within the project
    And there are no other developers assigned to the activity
    When the project manager removes the developer "BOSS" from the activity
    Then the error message "This developer is not assigned to the activity" is given