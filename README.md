This is Exam Project for 02161 Software Engineering I, group 09

An **important** thing to notice about the git-given contribution; there was many problems with our workflow. As a citation of the report: 

> "The work with git has not been as optimal as we’d hoped.  

We have encountered 
- merge- after merge error,
- git pulls who did not come through and 
- a git bash who simply gave up and made any commit other than a manual, individual impossible. 
- unlinked files who had not been touched prior to the commit

This is also why Jonathan (s204455) wasn’t (on git) contributing at first.  Jenifer (s204057) had JUnit problems and, since she was the most experienced with git helped with many of the unresolvable merge-errors. Both however, sent code to the others (then commited by the others) and helped debug and implement via screen-share on discord.  Simons (s204488) GitBash does, to date, not want to comply; he has to manually add each file manually in Gitlab’s online IDE. Fredrik (s204433) removed 56 files for a cleanup of the online repo manually. 

We do not feel this does our work-distribution honor.  _Everyone has contributed equally_. We would also like to state:  each group member has contributed to the project, the diagrams and the report equally.

**Instructions**


- Import the project by importing the "sofrwareengineeringi" folder as normal.
The folder contains all of the program code as well as all the tests.
- update project with maven before running the program. Right-click the project holder and click "maven update project". 
- To navigate the UI, simply type on of the shown commands. The options shown in <> represent
possible sub-options, simply choose one and leave out the <>.
- To run the program, simply right-click on the class name "Main.java"
found under src/main/java -> dtu.application.ui -> Main.java and run as Java application.
- To run the tests, simply rightclick on the project and click run as JUnit 5 test.
